<?php

use Illuminate\Database\Seeder;
use App\Models\Document;
use App\Models\Page;

class DocumentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		Document::create([
			'page_id' => Page::where('name', 'jedalen')->first()->id,
			'path'    => '#',
			'name'    => 'Plán práce'
		]);

		Document::create([
			'page_id' => Page::where('name', 'jedalen')->first()->id,
			'path'    => '#',
			'name'    => 'Školský poriadok'
		]);

		Document::create([
			'page_id' => Page::where('name', 'jedalen')->first()->id,
			'path'    => '#',
			'name'    => 'Štatút'
		]);
    }
}
