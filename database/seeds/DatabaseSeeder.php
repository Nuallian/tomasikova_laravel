<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call( AdminsTableSeeder::class );
        $this->call( PagesTableSeeder::class );
        $this->call( ContentTableSeeder::class );
        $this->call( DocumentsTableSeeder::class );
        $this->call( NoticesTableSeeder::class );
        $this->call( PhotoalbumsTableSeeder::class );

        Model::reguard();
    }
}
