<?php

use Illuminate\Database\Seeder;
use App\Models\Page;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Page::create([
            'name' => 'index',
            'fullname' => 'Uvítacia stránka'
        ]);

        Page::create([
            'name' => 'osporte',
            'fullname' => 'O športe'
        ]);

        Page::create([
            'name' => 'kontaktyzamestnanci',
            'fullname' => 'Kontakty zamestnancov'
        ]);

        Page::create([
            'name' => 'kontaktyskola',
            'fullname' => 'Kontakty školy'
        ]);

        Page::create([
            'name' => 'comenius',
            'fullname' => 'Comenius'
        ]);

        Page::create([
            'name' => 'comenius20122014',
            'fullname' => 'Comenius 2012 - 2014'
        ]);

        Page::create([
            'name' => 'kosickemoderneskoly',
            'fullname' => 'Košické moderné školy'
        ]);

        Page::create([
            'name' => 'cvc',
            'fullname' => 'Centrum voľného času'
        ]);

        Page::create([
            'name' => 'jedalen',
            'fullname' => 'Jedáleň'
        ]);

        Page::create([
            'name' => 'ziackyparlament',
            'fullname' => 'Žiacky parlament'
        ]);

        Page::create([
            'name' => 'cviceniazmatematiky',
            'fullname' => 'Cvičenia z matematiky'
        ]);

        Page::create([
            'name' => 'cviceniazinformatiky',
            'fullname' => 'Cvičenia z informatiky'
        ]);

        Page::create([
            'name' => 'skolskyporiadok',
            'fullname' => 'Školský poriadok'
        ]);

        Page::create([
            'name' =>'skolskyklubdeti',
            'fullname' => 'Školský klub detí'
        ]);

        Page::create([
            'name' => 'dennyrezimskoly',
            'fullname' => 'Denný režim školy'
        ]);

        Page::create([
            'name' => 'harmonogram',
            'fullname' => 'Harmonogram'
        ]);

        Page::create([
            'name' => 'zamestnanci',
            'fullname' => 'Zamestnanci'
        ]);

        Page::create([
            'name' => 'triedy',
            'fullname' => 'Triedy'
        ]);

        Page::create([
            'name' => 'rozvrhhodin',
            'fullname' => 'Rozvrh hodín'
        ]);

        Page::create([
            'name' => 'uspechyskoly',
            'fullname' => 'Úspechy školy'
        ]);

        Page::create([
            'name' => 'radaskoly',
            'fullname' => 'Rada školy'
        ]);

        Page::create([
            'name' => 'rodicovskezdruzenie',
            'fullname' => 'Rodičovské združenie'
        ]);

        Page::create([
            'name' => 'vychovneporadenstvo',
            'fullname' => 'Výchovné poradenstvo'
        ]);

        Page::create([
            'name' => 'plavaren',
            'fullname' => 'Plaváreň'
        ]);

        Page::create([
            'name' => 'dokumentyskoly',
            'fullname' => 'Dokumenty školy'
        ]);

        Page::create([
            'name' => 'verejneobstaravanie',
            'fullname' => 'Verejné obstarávanie'
        ]);

        Page::create([
            'name' => 'pracovnemiesta',
            'fullname' => 'Voľné pracovné miesta'
        ]);

        Page::create([
            'name' => 'prazdniny',
            'fullname' => 'Prázdniny'
        ]);

        Page::create([
            'name' => 'cistotatried',
            'fullname' => 'Čistota tried'
        ]);

        Page::create([
            'name' => 'aktivityskoly',
            'fullname' => 'Aktivity školy'
        ]);

        Page::create([
           'name' => 'oznamy',
           'fullname' => 'Oznamy'
        ]);

        Page::create([
           'name' => 'historiaskoly',
           'fullname' => 'História školy'
        ]);

        Page::create([
           'name' => 'oskole',
           'fullname' => 'O škole'
        ]);

        Page::create([
           'name' => 'zverejnovanie',
           'fullname' => 'Zverejňovanie'
        ]);

        Page::create([
           'name' => 'projektyskoly',
           'fullname' => 'Projekty školy'
        ]);

    }
}
