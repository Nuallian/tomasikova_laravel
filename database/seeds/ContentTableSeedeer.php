<?php

use Illuminate\Database\Seeder;
use App\Models\Content;

class ContentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Content::create([
            'model' => 'cvc',
            'body'  => '<h2><strong><span style="color:#006BC3">Centrum voľn&eacute;ho času</span></strong></h2><p><span style="color:#005EBC">V&nbsp;&scaron;kolskom roku&nbsp;2015/2016 bud&uacute;&nbsp;z&aacute;ujmov&eacute; &uacute;tvary združen&eacute;<br/>v CENTRE VOĽN&Eacute;HO ČASU PRI Z&Aacute;KLADNEJ &Scaron;KOLE,<br/>TOM&Aacute;&Scaron;IKOVA 31, 040 01 KO&Scaron;ICE</span></p><p><span style="color:#005EBC">Koordin&aacute;torom CVČ je Mgr. Miroslava Hr&iacute;bov&aacute; (ZR&Scaron; II. stupňa).</span></p><p><span style="color:#E26620"><u><strong>Kontakt:</strong></u><br/>telef&oacute;n: 055/ 633 18 03<br/>email: zstomhribova@gmail.com</span></p><p><span style="color:#005EBC">Dokument&aacute;cia CVČ sa nach&aacute;dza&nbsp;u&nbsp;riaditeľky &scaron;koly.</span></p><p><span style="color:#E26620"><u>Č&iacute;sla &uacute;čtov pre &uacute;hradu platieb za CVČ:</u></span> <span style="color:#005EBC"><strong>SK02 5600 0000 0005 0404 6002</strong></span><br/><span style="color:#005EBC">Prima banka Slovensko, a.s. m&aacute; pridelen&yacute;&nbsp;BIC k&oacute;d:&nbsp;<strong>KOMASK2X</strong><br/>Označenie banky:&nbsp;<strong>PKBA</strong></span></p><p><span style="color:#E26620"><u>Pri platb&aacute;ch uv&aacute;dzajte:</u></span><br/><span style="color:#005EBC">Variabiln&yacute; symbol:&nbsp;<strong>09502</strong><br/>Spr&aacute;va pre prij&iacute;mateľa:&nbsp;<strong>Meno a&nbsp;priezvisko dieťaťa, trieda</strong><br/>Splatnosť:&nbsp;<strong>do 15. dňa mesiaca</strong></span></p>'
        ]);

        Content::create([
            'model' => 'jedalen',
            'body'  => '<h2>Školská jedáleň</h2><p class="orange">Tel. číslo <span class="bold">055/ 63 32 271</span><br/></p><p>Cena obeda na I. stupni: <span class="bold">1,01 €</span> <br/>Cena obeda na II. stupni: <span class="bold">1,09 €</span> <br/>Forma platenia: <span class="bold">šekom, bankovým prevodom</span> <br/></p><p><span class="orange underlined">Odhlásenie dieťaťa z obeda:</span><br/>1. telefonicky: 1 deň vopred do 8.00 hod. <br/>/v prípade náhleho ochorenia – ráno do 8.00 hod. <br/>2. elektronicky: cez záložku jedáleň IZK <br/></p><p><span class="orange underlined">Číslo účtu pre úhradu platieb za stravu v školskej jedálni:</span><span class="bold"> SK63 5600 0000 0005 0404 8008 </span><br/>Prima banka Slovensko, a. s. má pridelený BIC kód: <span class="bold">KOMASK2X</span><br/>Označenie banky: <span class="bold">PKBA</span><br/></p>'
        ]);

        Content::create([
            'model' => 'ziackyparlament',
            'body'  => '<h2>Žiacky parlament</h2><p>V šk. roku 2015/2016 bude žiacky parlament pracovať v tomto zložení: <br/></p>'
        ]);

        Content::create([
            'model' => 'cviceniazmatematiky',
            'body'  => '<h2>Cvičenia z matematiky</h2>'
        ]);

        Content::create([
            'model' => 'skolskyklubdeti',
            'body' => '<h2>Školský klub detí</h2>'
        ]);
    }
}
