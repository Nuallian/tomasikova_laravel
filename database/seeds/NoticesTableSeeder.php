<?php

use Illuminate\Database\Seeder;
use App\Models\Notice;
use App\Models\Page;

class NoticesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Notice::create([
    		'page_id' => Page::where('name', 'cvc')->first()->id,
    		'content' => '<h2 class="deco">Milí žiaci!</h2><p>Lorem Ipsum dolor sit amet</p>'
		]);
    }
}
