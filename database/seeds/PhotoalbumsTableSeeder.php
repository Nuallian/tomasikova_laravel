<?php

use Illuminate\Database\Seeder;
use App\Models\Photoalbum;

class PhotoalbumsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		Photoalbum::create([
			'name' => 'fotoalbum_1',
			'year' => '2015/2016'
		]);

		Photoalbum::create([
			'name' => 'fotoalbum_2',
			'year' => '2015/2016'
		]);

		Photoalbum::create([
			'name' => 'fotoalbum_3',
			'year' => '2014/2015'
		]);
    }
}
