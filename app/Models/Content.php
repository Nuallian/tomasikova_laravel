<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
	protected $table = 'content';
	public $timestamps = false;

	// relationships
	public function page() {
		return $this->belongsTo('Page');
	}
}
