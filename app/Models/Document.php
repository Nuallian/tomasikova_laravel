<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
	protected $table   = 'documents';
	public $timestamps = false;

	// Relationships
	public function page() {
		return $this->belongsTo('App\Models\Page');
	}
}
