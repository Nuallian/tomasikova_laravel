<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notice extends Model
{
	protected $table   = 'notices';
	public $timestamps = false;

    // relationships
    public function page() {
    	return $this->belongsTo('App\Models\Page');
    }
}
