<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    /**
     * Table name for the model.
     * 
     * @var string
     */
    protected $table   = 'articles';
    
    /**
     * Include timestamps for this model.
     * 
     * @var boolean
     */
    public $timestamps = true;
}
