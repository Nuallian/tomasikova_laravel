<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Image;
use App\Models\Photoalbum;

class Photoalbum extends Model
{
	protected $table   = 'photoalbums';
	public $timestamps = false;

	// Photoalbum functions
	public function hasImages($id) {
		if (count(Image::where('photoalbum_id', $id)->get()) == 0) {
			return false;
		}
		return true;
	}

	public function getPreviewPhoto($id) {
		$image = Image::where('photoalbum_id', $id)->firstOrFail();
		return $image->filename . '.jpg';
	}

	public function returnYearForUrl($album_id) {
		$album = Photoalbum::where('id', $album_id)->firstOrFail();
		return str_replace('/', '-', $album->year);
	}

	// Relationships
	public function photo() {
		return $this->hasMany('App\Models\Image');
	}
}
