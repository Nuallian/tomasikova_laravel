<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticable;

class Admin extends Authenticable
{
	protected $table      = 'admins';
	protected $primaryKey = 'id';
	public $timestamps    = false;

	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = ['email', 'password'];

	/**
	* The attributes excluded from the model's JSON form.
	*
	* @var array
	*/
	protected $hidden = ['password', 'remember_token'];
}
