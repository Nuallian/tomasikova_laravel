<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Publication extends Model
{
    protected $table   = 'publications';
    public $timestamps = true;

    /**
     * Attributes that pass the mass assignment exception.
     * 
     * @var array $fillable
     */
    protected $fillable = [
        'publication_type', 'year', 'contract_number', 'date', 'contractor', 'contractor_address', 'contract_subject', 'internal_bill_number', 'bill_arrival_date', 'ico_number', 'fullfilment_description', 'price', 'identifier', 'date_of_completion', 'responsible_person_credentials', 'note'
    ];

    /**
     * Returns the full name of the publication type.
     * 
     * @return string
     */
    public function getTypeFullname() {
        $publication = Publication::where('id', $this->attributes['id'])->firstOrFail();

        ($publication->publication_type == 'order' ? $name = 'Objednávky' : NULL);
        ($publication->publication_type == 'canteen' ? $name = 'Jedáleň' : NULL);
        ($publication->publication_type == 'contract' ? $name = 'Zmluvy' : NULL);
        ($publication->publication_type == 'bill' ? $name = 'Faktúry' : NULL);

        return $name;
    }

    /**
     * Gets the name for the visualisation page.
     * 
     * @return string
     */
    public function getNameForVisualisation() {
        $publication = Publication::where('id', $this->attributes['id'])->firstOrFail();

        ($publication->publication_type == 'order' ? $name = 'Objednávky ZŠ' : NULL);
        ($publication->publication_type == 'canteen' ? $name = 'Faktúry ŠJ' : NULL);
        ($publication->publication_type == 'contract' ? $name = 'Zmluvy ZŠ' : NULL);
        ($publication->publication_type == 'bill' ? $name = 'Faktúry ZŠ' : NULL);

        return $name;
    }
}
