<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Document;
use App\Models\Notice;

class Page extends Model
{
	protected $table   = 'pages';
	public $timestamps = false;

	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = ['name', 'fullname'];

	/**
	 * Checks if the page is able to be bound with a new document
	 * 
	 * @param  int
	 * @return boolean
	 */
	public function isExtendableByDocument($page_id) {
		$notices   = Notice::where('page_id', $page_id)->get();
		if ( $notices->isEmpty() == true) {
			return true;
		}
		else {
			return false;
		}		
	}

	/**
	 * Checks if the page has any notices or documents associated with it
	 * 
	 * @param  int
	 * @return boolean
	 */
	public function hasNoNoticeOrDocuments($page_id) {
		$documents = Document::where('page_id', $page_id)->get();
		$notices   = Notice::where('page_id', $page_id)->get();

		if ( $documents->isEmpty() == true && $notices->isEmpty() == true ) {
			return true;
		}
		else {
			return false;
		}
	}

	// Relationships
	public function content() {
		return $this->hasMany('App\Models\Content');
	}

	public function document() {
		return $this->hasMany('App\Models\Document');
	}

	public function notice() {
		return $this->hasOne('App\Models\Notice');
	}
}
