<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Publicationfile extends Model
{
    /**
     * Table for the model.
     * 
     * @var string
     */
    protected $table   = 'publicationfiles';

    /**
     * Timestamps definition for the model.
     * 
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Relationships
     */
    public function publication() {
        return $this->belongsTo('App\Models\Publication');
    }
}
