<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
	protected $table   = 'images';

	// relationships
	public function photoalbum() {
		return $this->belongsTo('App\Models\Photoalbum');
	}
}
