<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Config;

// custom app models
use App\Models\Page;
use App\Models\Content;
use App\Models\Document;
use App\Models\Notice;
use App\Models\Photoalbum;
use App\Models\Carouselimage;
use App\Models\Image;
use App\Models\File;
use App\Models\Article;

class AdminController extends Controller
{
	/**
	 * Constructor for the admin controller.
	 */
	public function __construct() {
		$this->middleware('auth');
	}

	/**
	 * Shows the admin dashboard.
	 * 
	 * @param  string $category
	 * @return view
	 */
	public function showDashboard() {
		$pages     = Page::all();
		$documents = Document::all();
		$albums    = Photoalbum::orderBy('year','desc')->get();
		$notices   = Notice::all();
		$files     = File::all();
		$articles  = Article::all();
		
		return view('admin.dashboard', compact("pages", "albums", "documents", "notices", "files", "articles", "stuff"));
	}

	/**
	 * Fetch the whole collection of an admin model.
	 * 
	 * @param  string $category
	 * @return json
	 */
	public function fetchAdminCategory(Request $request) {
		$category = $_POST["category"];
		unset($fetchedData);

		$fetchedData;

		$category == "Page" ? $fetchedData = Page::all() : $fetchedData == [];
		$category == "Document" ? $fetchedData = Document::all() : $fetchedData == [];
		$category == "Album" ? $fetchedData = Photoalbum::orderBy('year','desc')->get() : $fetchedData == [];
		$category == "Notice" ? $fetchedData = Notice::all() : $fetchedData == [];

		return response()->json([$category => $fetchedData]);
	}

	public function editPage($page) {
		$model     = Page::where('name', $page)->first();
		$content   = Content::where('model', $page)->first();
		$notice    = Notice::where('page_id', $model->id)->first();
		$documents = Document::where('page_id', $model->id)->get();

		return view('admin.pages.edit', compact("model", "content", "notice", "documents"));
	}

	public function save(Request $request) {

		$new_content   = $request->contents_for_saving;
		$model_name    = $request->model_name;


		$content = Content::where('model', $model_name)->first();
		
		if ($content == null) {
			$content = new Content();
			$content->model = $model_name;
		}

		$content->body = $new_content;
		$content->save();

		return redirect()->route('dashboard');
	}

	public function createAlbum() {
		return view('admin.photos');
	}

	public function storeAlbum() {

		$input = Input::all();

		if ($input['album_name'] == "" || $input['album_year'] == "") {
			return redirect()->back();
		}

		$album = new Photoalbum();
		$album->name = $input['album_name'];
		$album->year = $input['album_year'];
		$album->save();

		return redirect()->route('dashboard');
	}

	public function editAlbum($name) {
		$album  = Photoalbum::where('name', $name)->first();
		$images = Image::where('photoalbum_id', $album->id)->get();

		return view('admin.album', compact("album", "images"));
	}

	public function deleteAlbum($id) {
		$albumToDelete = Photoalbum::where('id', $id)->first();
		$photos = Image::where('photoalbum_id', $id)->get();

		// delete all the photos from the album
		for ($i=0; $i < count($photos) ; $i++) { 
			// delete the reference from the database
			Image::destroy($photos[$i]->id);

			// delete them from the server
			$path            = public_path(Config::get('images.full_size_upload_dir')) . $photos[$i]->filename . '.jpg';
			$path_thumb      = public_path(Config::get('images.thumb_upload_dir')) . $photos[$i]->filename . '.jpg';
			$delete_fullsize = unlink($path);
			$delete_thumbs   = unlink($path_thumb);
		}

		// delete the album reference
		if ($albumToDelete) {
			$delete = Photoalbum::destroy($albumToDelete->id);

			if ($delete == true) {
				return redirect()->route('dashboard');
			}
			else {
				return redirect()->back();
			}
		}
	}
}
