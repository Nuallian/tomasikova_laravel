<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Repositories\NoticeRepository;
use App\Models\Page;
use App\Models\Notice;

class NoticeController extends Controller
{
	public function __construct(NoticeRepository $noticerepository) {
		$this->repository = $noticerepository;
		$this->middleware('auth');
	}

	public function create() {
		$pages = Page::all();
		return view('admin.notices.create', compact("pages"));
	}

	public function edit($id) {
		$notice = Notice::where('id', $id)->firstOrFail();
		$pages  = Page::all();

		return view('admin.notices.edit', compact("notice", "pages"));
	}

	public function store(Request $request) {
		$result = $this->repository->store($request);

		if ($result->getStatusCode() == 200) {
		 	return redirect()->route('dashboard');
		}
	}

	public function update($id, Request $request) {
		$result = $this->repository->update($id, $request);

		if ($result->getStatusCode() == 200) {
			return redirect()->route('dashboard');
		}
	}

	public function destroy($id) {
		$this->repository->destroy($id);
		return redirect()->route('dashboard');
	}
}
