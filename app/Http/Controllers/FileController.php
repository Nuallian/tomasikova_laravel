<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;

use App\Repositories\FileRepository;
use App\Models\Page;
use App\Models\File;

class FileController extends Controller {

	public function __construct(FileRepository $fileRepository) {
		$this->repository = $fileRepository;
		$this->middleware('auth');
	}

	public function create() {
		$pages = Page::all();
		return view('admin.files.create', compact("pages"));
	}

	public function store() {
		$input    = Input::all();
		$response = $this->repository->upload($input);

		if ($response->getStatusCode() == 200) {
			return redirect()->route('dashboard');
		}
	}

	public function destroy($file_id) {
		$response = $this->repository->delete($file_id);
		return redirect()->route('dashboard');
	}

}
