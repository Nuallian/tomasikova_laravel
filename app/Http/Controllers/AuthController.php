<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use App\User;
use Auth;

class AuthController extends Controller
{
    public function login(Request $request) {
        $email    = $request->email;
        $password = $request->password;

        if (Auth::attempt(['email' => $email, 'password' => $password], true)) {
            return redirect()->route('dashboard');
        }
        else {
            return redirect()->back();
        }
    }

    public function logout() {
        Auth::logout();
        return redirect('/auth');
    }

}
