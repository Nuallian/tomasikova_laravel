<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

use App\Repositories\ImageRepository;
use App\Repositories\TextImageRepository;
use App\Models\Carouselimage;

class ImageController extends Controller
{
    public function __construct(ImageRepository $imageRepository, TextImageRepository $textImageRepository) {
        $this->image     = $imageRepository;
        $this->textImage = $textImageRepository;
        $this->middleware('auth');
    }

    public function editCarousel() {
        $images = Carouselimage::all();
        return view('admin.carousel', compact("images"));
    }

    public function postUpload(Request $request) {
    	$path = $request->path();
    	$path = str_replace('/', '', $path);
    	$path = str_replace('store', '', $path);

        $photo    = Input::all();
        $album_id = Input::get('photoalbum_id', 0);

    	$response = $this->image->upload($photo, $path, $album_id);
    	return $response;
    }

    public function delete($imageName, $type) {
        $response = $this->image->delete($imageName, $type);
        
        if ($response == true) {
            return redirect()->back();
        }

        else {
            return $response;
        }
    }

    public function uploadTextImage() {
        $response = $this->textImage->save(Input::all());

        if ($response->getStatusCode() == 200) {
            return $response;
        }
        else {
            return response()->json([
                "uploaded" => 0,
                "error"    => "Pri uploadovaní obrázku nastala niekde chyba. Kontaktujte správcu systému"
            ]);
        }
    }
}
