<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

use App\Repositories\DocumentRepository;
use App\Models\Page;

class DocumentController extends Controller
{
	public function __construct(DocumentRepository $documentRepository) {
		$this->repository = $documentRepository;
		$this->middleware('auth');
	}

	public function create() {
		$pages = Page::all();
		return view('admin.documents.create', compact("pages"));
	}

	public function store(Request $request) {
		$form_data = Input::all();
		$document  =  $request->file('file');
		$response  = $this->repository->upload($form_data, $document[0]);

		if ($response->getStatusCode() == 200) {
			return redirect()->route('dashboard');
		}
	}

	public function edit($document_id) {
		$pages = Page::all();
		$document = $this->repository->getById($document_id);
		return view('admin.documents.edit', compact("document", "pages"));
	}

	public function update($document_id) {
		return 'Patching documet with the id of: ' . $document_id;
	}

	public function destroy($document_id) {
		$result = $this->repository->destroy($document_id);
		return redirect()->route('dashboard');
	}
}
