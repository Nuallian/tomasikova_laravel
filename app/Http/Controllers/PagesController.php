<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

// custom app models
use App\Models\Page;
use App\Models\Content;
use App\Models\Document;
use App\Models\Notice;
use App\Models\Carouselimage;
use App\Models\Photoalbum;
use App\Models\Image;
use App\Models\Publication;


class PagesController extends Controller
{

	public function matchTemplate(Request $request) {

		$uri         = $request->path();
		$template    = str_replace('-', '_', $uri);
		$model       = str_replace('-', '', $uri);
		$album_years = $this->getAlbumYears();
		$page        = Page::where('name', $model)->first();

		if ($template == '/') {
			$page           = Page::where('name', 'index')->first();
			$content        = Content::where('model', $page->name)->first();
			$carouselimages = Carouselimage::all();
			$documents      = Document::where('page_id', $page->id)->get();
			$notice         = Notice::where('page_id', $page->id)->first();

			return view('templates.index', compact("content", "documents", "notice", "album_years", "carouselimages", "page"));
		}

		else {

			if ($page == null) {
				return view('404');
			}

			$content = Content::where('model', $page->name)->first();
		}

		$documents      = Document::where('page_id', $page->id)->get();
		$notice         = Notice::where('page_id', $page->id)->first();

		return view('templates.page', compact("content", "documents", "notice", "album_years", "carouselimages", "page"));
	}

	public function showAlbums($year) {
		$album_years = $this->getAlbumYears();
		$year        = str_replace('-', '/', $year);
		$albums      = Photoalbum::where('year', $year)->orderBy('id', 'desc')->paginate(20);

		return view('templates.album', compact("year", "albums", "album_years"));
	}

	public function showGallery($name) {
		$album       = Photoalbum::where('name', $name)->first();
		$photos      = Image::where('photoalbum_id', $album->id)->get();
		$album_years = $this->getAlbumYears();

		return view('templates.gallery', compact("album", "photos", "album_years"));
	}

	public function showLogin() {
		return view('admin.index');
	}

	function getAlbumYears() {

		$albums   = Photoalbum::all();

		$album_years = array();
		for ($i=0; $i < count($albums) ; $i++) {
				$year_formatted = str_replace('/', '-', $albums[$i]->year);
			if (!in_array($year_formatted, $album_years)) {
				array_push($album_years, $year_formatted);
			}
		}

		return $album_years;
	}

	/**
	 * Displays the index page for the publications
	 * 
	 * @return view
	 */
	public function displayPublicationsIndex() {
		$publications = Publication::orderBy('year', 'desc')->get();
		return view('templates.publications.index', compact("publications"));
	}

	/**
	 * Displays publications specified by type and year.
	 * 
	 * @param  string $year
	 * @param  string $type
	 * @return Illuminate\View
	 */
	public function displayPublications($year, $type) {
		$publication = Publication::where('year', $year)->where('publication_type', $type)->firstOrFail();

		return view('templates.publications.show', compact("publication"));
	}

	/**
	 * Display the archived publications
	 * 
	 * @param  string $year
	 * @return view
	 */
	public function displayArchivedPublications($year, Request $request) {
		// get the type of the publication
		$type = $request->get('type');

		// return the view with the archived publication
		return view('templates.publications.archive.' . $year . '_' . $type);
	}

	/**
	 * Displays the landingpage.
	 * 
	 * @return view
	 */
	public function landingpage() {
		return view('templates.landingpage');
	}
}
