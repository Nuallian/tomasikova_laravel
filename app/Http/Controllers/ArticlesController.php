<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Article;

use App\Repositories\ImageRepository;
use App\Repositories\FileRepository;

use Illuminate\Database\Eloquent\ModelNotFoundException;

class ArticlesController extends Controller
{
    /**
     * Constructor function for this controller.
     */
    public function __construct(ImageRepository $imageRepository, FileRepository $fileRepository) {
        $this->imageRepository = $imageRepository;
        $this->fileRepository  = $fileRepository;
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

    /**
     * Display a list with all the articles.
     * 
     * @return view
     */
    public function index() {
        $articles = Article::orderBy('created_at', 'desc')->paginate(20);
        return view('articles.index', compact("articles"));
    }

    /**
     * Dispaly the form for creating a new article.
     * 
     * @return view
     */
    public function create() {
        return view('articles.create');
    }

    /**
     * Stores the newly created article to the database.
     * 
     * @param  Request $request 
     */
    public function store(Request $request) {
        // create an image path
        $img_path = $this->imageRepository->uploadArticleHeadingImage($request->file('image'));

        // create a database record
        $article           = new Article;
        $article->title    = $request->title;
        $article->body     = $request->contents_for_saving;
        $article->img_path = $img_path;
        $article->save();

        // redirect back
        return redirect()->route('dashboard');
    }

    /**
     * Shows the form for editing an article.
     * 
     * @param  int $article_id
     * @return view            
     */
    public function edit($article_id) {
        $article = Article::where('id', $article_id)->firstOrFail();
        return view('articles.edit', compact("article"));
    }

    /**
     * Updates the article.
     * 
     * @param  int $article_id
     */
    public function update($article_id, Request $request) {
        $article = Article::where('id', $article_id)->firstOrFail();
        // create an image path, if there is any image
        if ($request->hasFile('image')) {
            $this->fileRepository->safeDeleteFile($article->img_path);
            $img_path          = $this->imageRepository->uploadArticleHeadingImage($request->file('image'));
            $article->img_path = $img_path;
        }

        $article->title = $request->title;
        $article->body  = $request->contents_for_saving;
        $article->save();

        return redirect()->route('dashboard');
    }

    /**
     * Deletes an article and image asscoaited with it as well.
     * 
     * @param  int $article_id
     * @return redirect
     */
    public function destroy($article_id) {
        try {
            $article = Article::where('id', $article_id)->firstOrFail();
            $this->fileRepository->safeDeleteFile($article->img_path);
            $article->delete();
        } catch (ModelNotFoundException $e) {
            return redirect()->back();
        }

        return redirect()->back();
    }
}
