<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Contracts\Logging\Log;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Publication;
use App\Models\Publicationfile;

use App\Repositories\FileRepository;

use Illuminate\Database\Eloquent\ModelNotFoundException;

class PublicationsController extends Controller
{
    /**
     * Constructor for this controller.
     * 
     */
    public function __construct(FileRepository $fileRepository) {
        // set up the middleware for this controller
        $this->middleware('auth');

        // inject the repositories
        $this->fileRepository        = $fileRepository;
    }

    public function index($year = NULL) {
        $contracts    = Publication::where('publication_type', 'contract')->where('year', $year)->get();
        $bills        = Publication::where('publication_type', 'bill')->where('year', $year)->get();
        $orders       = Publication::where('publication_type', 'order')->where('year', $year)->get();
        $canteens     = Publication::where('publication_type', 'canteen')->where('year', $year)->get();        
        $years        = Publication::groupBy('year')->get(['year']);
        $current_year = $year;

        return view('publications.index', compact("contracts", "bills", "orders", "canteens", "years", "current_year"));
    }

    public function create($type) {
        return view('publications.create', compact("type"));
    }

    public function edit($publication_id) {
        $publication = Publication::where('id', $publication_id)->firstOrFail();
        return view('publications.edit', compact("publication"));
    }

    public function store(Request $request) {
        $publication = Publication::create(Input::except(['_token']));

        return $publication->id;
    }

    public function storeHtml(Request $request) {
        // get stuff from the form
        $file = $request->file('file');
        $year = $request->input('year');
        $type = $request->input('publication_type');

        // validate for the file
        if ( ! $file) {
            return redirect()->back();
        }

        // determine if this is the first file for this year and this publication
        try {
            $publication = Publication::where('publication_type', $type)->where('year', $year)->firstOrFail();

            // delete the old template of the publication for this type of publication and this year if it is present
            $this->fileRepository->safeDeleteFile($publication->path);
        } catch (ModelNotFoundException $e) {
            $publication = new Publication;
        }

        // adjust the html
        $newFile = $this->fileRepository->adjustHtml($file);

        // upload the new file
        $path = $this->fileRepository->createPublicationFile($newFile->saveHtml(), $file, $year, $type);
        
        // make a reference to the new file in the database
        $publication->path             = $path;
        $publication->publication_type = $type;
        $publication->year             = $year;
        $publication->save();

        // redirect back to index
        return redirect('/publication');
    }

    /**
     * Display the files for the current year.
     * 
     * @param  int $year
     * @return view
     */
    public function fileManager() {
        $files = Publicationfile::all();
        $years = Publicationfile::groupBy('year')->get();

        return view('publications.file-manager', compact("files", "years"));
    }

    /**
     * Filters the recipes.
     * 
     * @param  Request $request
     * @return Object
     */
    public function filter(Request $request) {
        $year         = $request->get('year');
        $publications = Publicationfile::where('year', $year)->get();

        return response()->json($publications);
    }

    /**
     * Stores the file and associates it with the given publication.
     * 
     * @param  Request $request
     * @return App\Models\File
     */
    public function storeFile(Request $request) {
        // get the file
        $file = $request->file('file');
        $year = $request->get('year');

        // store it on the hard drive
        $path = $this->fileRepository->storeBillFile($file, $year);

        // get the publication id and associate the file with the publication
        $publication_file       = new Publicationfile;
        $publication_file->year = $year;
        $publication_file->name = $file->getClientOriginalName();
        $publication_file->path = $path;
        $publication_file->save();

        // return success
        return response()->json([], 200);
    }

    /**
     * Deletes the file and unlinks it from a publication.
     * 
     * @param  [type] $file_id [description]
     * @return [type]          [description]
     */
    public function deleteFile($file_id) {
        // get the publication file
        $publication = Publicationfile::where('id', $file_id)->firstOrFail();

        // delete it physically
        $this->fileRepository->safeDeleteFile($publication->path);

        // delete it from the database
        Publicationfile::destroy($file_id);

        // return back to editing that publication
        return redirect()->back();
    }

    public function destroy($id) {
        Publication::destroy($id);
        return redirect('/publication');
    }
}
