<?php

// Authentication routes
Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('/landingpage', 'PagesController@landingpage');

    // ------------------------------------------------
    // Auth routes
    // ------------------------------------------------

    Route::get('/auth', 'PagesController@showLogin');
    Route::post('/auth', 'AuthController@login');
    Route::get('/logout', ['as' => 'logout', 'uses' => 'AuthController@logout']);
    Route::get('/admin/{category}', ['as' => 'dashboard', 'uses' => 'AdminController@showDashboard']);
    Route::get('/admin', ['as' => 'dashboard', 'uses' => 'AdminController@showDashboard']);

    // ------------------------------------------------
    // Publications
    // ------------------------------------------------

    Route::get('/publication/{year}', 'PublicationsController@index');
    Route::get('/publicationsfiles', 'PublicationsController@fileManager');
    Route::resource('/publication', 'PublicationsController', ['except' => ['destroy']]);
    Route::get('/publication/{id}/destroy', 'PublicationsController@destroy');
    Route::get('/publication/create/{type}', 'PublicationsController@create');
    Route::post('/publication/filter', 'PublicationsController@filter');
    Route::post('/publication/storeFiles', 'PublicationsController@storeFile');
    Route::post('/publications/storeHtml', ['as' => 'publication.storeHtml', 'uses' => 'PublicationsController@storeHtml']);
    Route::get('/publicationfile/delete/{file_id}', 'PublicationsController@deleteFile');


    // ------------------------------------------------
    // Editing models
    // ------------------------------------------------

    // Documents
    Route::get('/document/{document_id}/destroy', 'DocumentController@destroy');
    Route::resource('document', 'DocumentController', ['except' => [ 'index', 'show', 'destroy' ]]);

    // Notices
    Route::get('/notice/{notice_id}/destroy', 'NoticeController@destroy');
    Route::resource('notice', 'NoticeController', ['except' => [ 'index', 'show', 'destroy' ]]);

    // Files
    Route::get('/file/{file_id}/destroy', 'FileController@destroy');
    Route::resource('file', 'FileController', ['except' => [ 'index', 'show', 'destroy' ]]);

    // Photos and Carousel photos
    Route::get('/carousel/update', 'ImageController@editCarousel');
    Route::post('/carousel/store', 'ImageController@postUpload');
    Route::get('/image/delete/{name}/{type}', 'ImageController@delete');
    Route::post('/textimage/store', 'ImageController@uploadTextImage');

    // Photoalbums
    Route::get('/album/create', 'AdminController@createAlbum');
    Route::post('/album/store', 'AdminController@storeAlbum');
    Route::get('/album/{name}/edit', 'AdminController@editAlbum');
    Route::get('/album/{id}/delete', 'AdminController@deleteAlbum');
    Route::post('/photo/store', 'ImageController@postUpload');

    // Articles
    Route::resource('article', 'ArticlesController', ['except' => ['destroy']]);
    Route::get('/article/{id}/destroy', 'ArticlesController@destroy');

    // Content of pages
    Route::get('/{page}/edit', 'AdminController@editPage');
    Route::post('/{page}/save', 'AdminController@save');
});

// Albums and Galleries manipulation
Route::get('/albums/{year}', 'PagesController@showAlbums');
Route::get('/gallery/{name}', 'PagesController@showGallery');

// Displaying publications (sudo code for the old ones when possible)
Route::group(['prefix' => '/zverejnovanie'], function() {
    Route::get('/', 'PagesController@displayPublicationsIndex');
    Route::get('/archiv/{rok}', 'PagesController@displayArchivedPublications');
    Route::get('/display/{year}/{type}', 'PagesController@displayPublications');
});

// New form for acitvities
Route::get('/temp/aktivity', 'ArticlesController@index');

// Matches URL to Content
Route::get('/', 'PagesController@matchTemplate' );
Route::get('/{template}', 'PagesController@matchTemplate');
