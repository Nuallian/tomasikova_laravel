<?php

namespace App\Repositories;

use App\Models\Image;
use App\Models\Carouselimage;

use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManager;

class TextImageRepository {

	public function save($input) {
		$image = $input['upload'];

		$original_name = $image->getClientOriginalName();
		$unique_name   = $this->createUniqueFilename($original_name, NULL);

		$directory     = Config::get('textimage.upload_dir');
		$image->move($directory, $unique_name);

		if ($image) {
			return response()->json([
				"uploaded" => 1,
				"fileName" => $unique_name,
				"url"      => $directory . $unique_name,
				"code"     => 200
			]);
		}
		else {
			return response()->json([ "code" => 500 ]);
		}
	}

	private function createUniqueFilename($original_name) {

		$directory     = public_path(Config::get('textimage.upload_dir'));
		$document_path = $directory . '/' . $original_name;

		if ( File::exists($document_path) )
		{
		    // Generate token for the file
		    $token = substr(sha1(mt_rand()), 0, 5);
		    $orignal_name_without_ext = substr($original_name, 0, strlen($original_name) - 4);
		    return $orignal_name_without_ext . '-' . $token . '.png';
		}

		return $original_name;
	}

}
