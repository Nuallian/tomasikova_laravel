<?php

namespace App\Repositories;

use App\Models\Document;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;

class DocumentRepository {

	public function upload($form_data, $file) {
		$file          = $file;

		$original_name = $file->getClientOriginalName();
		$unique_name   = $this->createUniqueFilename($original_name);

		$directory = public_path(Config::get('documents.upload_dir'));
		$file->move($directory, $unique_name);

		$new_document          = new Document();
		$new_document->page_id = $form_data['page_id'];
		$new_document->name    = $form_data['document_name'];
		$new_document->path    = 'documents/' . $unique_name;
		$new_document->save();

		if ($new_document && $file) {
			return response()->json([ 'error' => false, 'code' => 200 ]);
		}
	}

	public function getById($id) {
		$document = Document::where('id', $id)->first();
		return $document;
	}

	public function destroy($id) {
		$document = Document::where('id', $id)->first();
		unlink($document->path);
		Document::destroy($id);
	}

	private function createUniqueFilename($original_name) {

		$directory     = public_path(Config::get('documents.upload_dir'));
		$document_path = $directory . '/' . $original_name;

		if ( File::exists($document_path) )
		{
			$pathinfo  = pathinfo($original_name);
			$extension = $pathinfo['extension'];

		    // Generate token for the file
		    $token = substr(sha1(mt_rand()), 0, 5);
		    $orignal_name_without_ext = substr($original_name, 0, strlen($original_name) - 4);
		    return $orignal_name_without_ext . '-' . $token . '.' . $extension;
		}

		return $original_name;
	}

}
