<?php

namespace App\Repositories;

use App\Models\Notice;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;

class NoticeRepository {

	public function store($form) {
		$notice = new Notice;
		$notice->page_id = $form->page_id;
		$notice->content = $form->contents_for_saving;
		$notice->save();

		if ($notice) {
			return response()->json(["code" => 200]);
		}
	}

	public function update($id, $form) {
		$notice = Notice::where('id', $id)->first();
		$notice->page_id = $form->page_id;
		$notice->content = $form->contents_for_saving;
		$notice->save();

		if ($notice) {
			return response()->json(["code" => 200]);
		}
	}

	public function destroy($id) {
		Notice::destroy($id);
	}

}
