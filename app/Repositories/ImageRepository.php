<?php

namespace App\Repositories;

use App\Models\Image;
use App\Models\Carouselimage;

use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManager;

class ImageRepository {

	public function upload($form_data, $type, $album_id) {

		$photo = $form_data['file'];

		$originalName = $photo->getClientOriginalName();
		$originalNameWithoutExt = substr($originalName, 0, strlen($originalName) - 4);

		$filename = $this->sanitize($originalNameWithoutExt);
		$allowed_filename = $this->createUniqueFilename( $filename, $type );

		$filenameExt = $allowed_filename .'.jpg';

		$uploadSuccess1 = $this->original( $photo, $filenameExt, $type );

		if ($type == "carousel") {

			if( !$uploadSuccess1 ) {
				return Response::json([
				    'error' => true,
				    'message' => 'Server error while uploading',
				    'code' => 500
				], 500);
			}
		}

		else {
			
			$uploadSuccess2 = $this->icon( $photo, $filenameExt );

			if( !$uploadSuccess1 || !$uploadSuccess2 ) {

			    return Response::json([
			        'error' => true,
			        'message' => 'Server error while uploading',
			        'code' => 500
			    ], 500);

			}
		}

		if ($type == "carousel") {
			$sessionImage       = new Carouselimage();
			$sessionImage->name = $allowed_filename;
		}

		else {
			$sessionImage                = new Image();
			$sessionImage->filename      = $allowed_filename;
			$sessionImage->original_name = $originalName;
			$sessionImage->photoalbum_id = $album_id;
		}

		$sessionImage->save();

		return Response::json([
		    'error' => false,
		    'code'  => 200
		], 200);
	}

	public function createUniqueFilename( $filename, $type )
	{
		if ($type == "carousel") {
			$full_size_dir = public_path(Config::get('images.carousel_upload_dir'));
		}

		else {
			$full_size_dir = public_path(Config::get('images.full_size_upload_dir'));
		}
	    $full_image_path = $full_size_dir . $filename . '.jpg';

	    if ( File::exists( $full_image_path ) )
	    {
	        // Generate token for image
	        $imageToken = substr(sha1(mt_rand()), 0, 5);
	        return $filename . '-' . $imageToken;
	    }

	    return $filename;
	}

	/**
	 * Optimize Original Image
	 */
	public function original( $photo, $filename, $type )
	{
	    $manager = new ImageManager();

	    if ($type == "carousel") {
	    	$image = $manager->make( $photo )->encode('jpg')->save(public_path(Config::get('images.carousel_upload_dir')) . $filename, 60 );
	    }
	    else {
			$image = $manager->make( $photo )->encode('jpg')->save(public_path(Config::get('images.full_size_upload_dir')) . $filename, 60 );
	    }

	    return $image;
	}

	/**
	 * Create Icon From Original
	 */
	public function icon( $photo, $filename )
	{
	    $manager = new ImageManager();
	    $image = $manager->make( $photo )->encode('jpg')->resize(200, null, function($constraint){$constraint->aspectRatio();})->save( public_path(Config::get('images.thumb_upload_dir')) . $filename, 60 );

	    return $image;
	}

	public function delete($imageName, $type) {

		if ($type == "carousel") {

			// remove the record from database
			$imageToDelete = Carouselimage::where('name', $imageName)->first();
			Carouselimage::destroy($imageToDelete->id);

			// deletes the photo from the server
			$path = public_path(Config::get('images.carousel_upload_dir')) . $imageToDelete->name . '.jpg';
			$delete = unlink($path);

			if ($delete == true) {
				return true;
			}

			else {
				return false;
			}
		}

		else {
			// remove the record from database
			$imageToDelete = Image::where('filename', $imageName)->first();
			Image::destroy($imageToDelete->id);

			// deletes the photo and the thumbnail from the server
			$path       = public_path(Config::get('images.full_size_upload_dir')) . $imageToDelete->filename . '.jpg';
			$path_thumb = public_path(Config::get('images.thumb_upload_dir')) . $imageToDelete->filename . '.jpg';
			$delete     = unlink($path);
			$delete     = unlink($path_thumb);

			if ($delete == true) {
				return true;
			}

			else {
				return false;
			}
		}

	}

	/**
	 * Uploads the little haeding image for an article.
	 * 
	 * @return string
	 */
	public function uploadArticleHeadingImage($image) {
		$manager = new ImageManager();

		$base_path = Config::get('images.article_img_upload_dir');
		$token     = substr(sha1(mt_rand()), 0, 24);

		$manager->make($image)->encode('gif')->save(public_path($base_path . $token . '.gif'), 60);

		return $base_path . $token . '.gif';
	}

	function sanitize($string, $force_lowercase = true, $anal = false)
	{
	    $strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]",
	        "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
	        "â€”", "â€“", ",", "<", ".", ">", "/", "?");
	    $clean = trim(str_replace($strip, "", strip_tags($string)));
	    $clean = preg_replace('/\s+/', "-", $clean);
	    $clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean ;

	    return ($force_lowercase) ?
	        (function_exists('mb_strtolower')) ?
	            mb_strtolower($clean, 'UTF-8') :
	            strtolower($clean) :
	        $clean;
	}

}
