<?php

namespace App\Repositories;

use App\Models\File as NewFile;

use League\Csv\Reader;
use League\Csv\Writer;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;

class FileRepository {

	/**
	 * Base path for storing publication templates.
	 * 
	 * @var string
	 */
	private $publication_files_base_path = 'publication_files';

	/**
	 * Base path for the bill uploaded files.
	 * 
	 * @var string
	 */
	private $bills_base_path = 'bills_files';

	/**
	 * The location of the publication files.
	 * 
	 * @var string
	 */
	private $publicationFileDirectory = 'publication_files';

	public function upload($input) {
		$file = $input['file'][0];

		$original_name = $file->getClientOriginalName();
		$unique_name   = $this->createUniqueFilename($original_name);

		$directory = public_path(Config::get('files.upload_dir'));
		$file->move($directory, $unique_name);

		$new_file       = new NewFile();
		$new_file->name = $input['file_name'];
		$new_file->path = 'files/' . $unique_name;
		$new_file->save();

		if ($file) {
			return response()->json([
				'error' => false,
				'code'  => 200
			]);
		}
	}

	public function delete($file_id) {
		// delete the file from the server
		$file = NewFile::where('id', $file_id)->firstOrFail();
		unlink($file->path);

		// remove the record of the file from the database
		NewFile::destroy($file_id);
	}

	private function createUniqueFilename($original_name) {
		$directory     = public_path(Config::get('files.upload_dir'));
		$file_path = $directory . '/' . $original_name;

		if ( File::exists($file_path) )
		{
			$pathinfo  = pathinfo($original_name);
			$extension = $pathinfo['extension'];

		    // Generate token for the file
		    $token = substr(sha1(mt_rand()), 0, 5);
		    $orignal_name_without_ext = substr($original_name, 0, strlen($original_name) - 4);
		    return $orignal_name_without_ext . '-' . $token . '.' . $extension;
		}

		return $original_name;
	}

	/**
	 * Stores the publication file on the disk.
	 * 
	 * @param  file $file
	 * @return string
	 */
	public function createPublicationFile($contents, $file, $year, $type) {
		$path = $this->publicationFileDirectory . '/' . substr(sha1(mt_rand()), 0, 20) . '-' . $type . '-' . $year . '.' . $file->getClientOriginalExtension();
		File::put($path, $contents);
		return $path;
	}

	/**
	 * Stores a bill physically on the drive.
	 * 
	 * @param  file $file
	 * @return string
	 */
	public function storeBillFile($file, $year) {
		$name = $file->getClientOriginalName();
		$path = $this->bills_base_path . '/' . $year . '/';

		$file->move($path, $name);
		return $path . $name;
	}

	/**
	 * Adjusts the html to internal rules.
	 * 
	 * @param  string $file
	 * @return Object $doc
	 */
	public function adjustHtml($file) {
		// create a new DomDocument object
		$doc = new \DOMDocument();

		// load the HTML into the DomDocument object and remove the style tags
		@$doc->loadHTML(file_get_contents($file));
		$this->removeElementsByTagName('style', $doc);


		// add internal classes to the table
		$tables = $doc->getElementsByTagName('table');
		foreach ($tables as $table) {
			$table->setAttribute('class', 'dynamic_table minimal');
		}

		return $doc;
	}

	/**
	 * Safely deletes the file from its physical storage.
	 * 
	 * @param  string $path
	 * @return mixed
	 */
	public function safeDeleteFile($path) {
		File::exists($path) ? File::delete($path) : NULL;
		return response()->json([], 200);
	}

	/**
	 * Removes nodes from html DOM.
	 * 
	 * @param  string $tagName
	 * @param  string $document
	 * @return
	 */
	private function removeElementsByTagName($tagName, $document) {
		$nodeList = $document->getElementsByTagName($tagName);
		for ($nodeIdx = $nodeList->length; --$nodeIdx >= 0; ) {
			$node = $nodeList->item($nodeIdx);
			$node->parentNode->removeChild($node);
		}
	}
}
