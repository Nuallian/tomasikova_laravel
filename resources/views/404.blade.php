@extends('layouts.page')

@section('content')

	<div class="divider"></div>

	<div class="wrapper">
		<h2>
			Táto stránka nemá žiaden obsah, alebo neexistuje
		</h2>
		<br>
		<a href="/">Vrátiť sa naspäť na index</a>
	</div>

@endsection
