@extends('layouts.page')

@section('head')
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('components/bootstrap/dist/css/bootstrap.min.css') }}"/>
@endsection

@section('content')

	<div class="divider"></div>

	<div class="wrapper">

		<ul class="gallery">
			@foreach($albums as $album)
				@if($album->hasImages($album->id))

					<a href="/gallery/{{ $album->name }}"><li>
						<div class="col-md-3">
							<div class="thumbnail album_hover">
								<div class="bg-gallery-img" style=" background-image: url( {{ URL::asset("photos/albums/thumbs/". $album->getPreviewPhoto($album->id)."") }} ); "></div>
								<div class="caption">
									<small>{{ $album->name }}</small>
								</div>
							</div>
						</div>
					</li></a>

				@endif
			@endforeach
		</ul>

		<div class="container">
		    <div class="row">
		        <div class="col-md-12 text-center">
		        	{!! $albums->links() !!}
		        </div>
		    </div>
		</div>

	</div>

@endsection
