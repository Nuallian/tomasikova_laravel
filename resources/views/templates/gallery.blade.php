@extends('layouts.page')

@section('content')

	<div class="divider"></div>

	<div class="wrapper">
		<div id="gallery">

			<a href="/albums/{{ $album->returnYearForUrl($album->id) }}" class="back_button">Spät k albumom</a>

			<h2>{{ $album->name }}</h2>

			@if (count($photos) != 0)

				@foreach($photos as $photo)

					<a href="{{ URL::asset('photos/albums/full_size/' . $photo->filename . '.jpg') }}" data-lightbox="album">
						<img src="{{ URL::asset('photos/albums/thumbs/' . $photo->filename . '.jpg') }}">
					</a>

				@endforeach
				
			@endif

		</div>
	</div>

@endsection
