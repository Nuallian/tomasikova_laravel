@extends('layouts.page')

@section('content')

	<div class="divider"></div>

	<div class="wrapper">

		@if(isset($documents) && count($documents) != 0)
			<div class="documents">

				<h2 class="deco">Dokumenty</h2>

					<ul id="documents-wrapper">
						@foreach($documents as $document)
							<a href="{{ URL::asset($document->path) }}" target="_blank">
								<li>{{ $document->name }}</li>
							</a>
						@endforeach
					</ul>
			</div>
		@endif

		@if (isset($notice) && count($notice) != 0)
			<div class="notice">
				<div class="documents-wrapper">

					{!! $notice->content !!}

					<img src="{{ URL::asset('img/clover.png') }}">

				</div>
			</div>
		@endif

		<div class="page">

			@if(isset($content) && count($content) != 0)

				{!! $content->body !!}

			@endif
		</div>

	</div>	

@stop
