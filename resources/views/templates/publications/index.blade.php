@extends('layouts.page')

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('components/bootstrap/dist/css/bootstrap.min.css') }}"/>
@endsection

@section('content')

    <div class="divider"></div>

    <div class="wrapper">
        <div class="container truncated">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        @include('templates.publications.partials.intro')

                        <div class="panel-body">
                            <strong>Zverejňujeme dokumenty:</strong><br><br>
                            <ul class="list-group">

                                <li class="list-group-item">
                                    <a href="http://www.eskoly.sk/tomasikova31/efaktury" target="_blank">Objednávky ŠJ</a>
                                </li>

                                @foreach($publications as $publication)
                                    <li class="list-group-item">
                                        <a href="
                                                {{ action('PagesController@displayPublications',
                                                ['year' => $publication->year, 'type' => $publication->publication_type]) }}
                                            "
                                        >
                                            Rok <span class="filter_year">{{ $publication->year }}</span> - {{ $publication->getNameForVisualisation() }}
                                        </a>
                                    </li>
                                @endforeach

                                <li class="list-group-item">
                                    <a href="{{ action('PagesController@displayArchivedPublications', ['year' => 2015, 'type' => 'school']) }}">
                                        Rok <span class="filter_year">2015</span> - Škola
                                    </a>
                                </li>
                                <li class="list-group-item">
                                    <a href="{{ action('PagesController@displayArchivedPublications', ['year' => 2015, 'type' => 'canteen']) }}">
                                        Rok <span class="filter_year">2015</span> - Faktúry ŠJ
                                    </a>
                                </li>

                                <li class="list-group-item">
                                    <a href="{{ action('PagesController@displayArchivedPublications', ['year' => 2014, 'type' => 'school']) }}">
                                        Rok <span class="filter_year">2014</span> - Škola
                                    </a>
                                </li>
                                <li class="list-group-item">
                                    <a href="{{ action('PagesController@displayArchivedPublications', ['year' => 2014, 'type' => 'canteen']) }}">
                                        Rok <span class="filter_year">2014</span> - Faktúry ŠJ
                                    </a>
                                </li>

                                <li class="list-group-item">
                                    <a href="{{ action('PagesController@displayArchivedPublications', ['year' => 2013, 'type' => 'school']) }}">
                                        Rok <span class="filter_year">2013</span> - Škola
                                    </a>
                                </li>
                                <li class="list-group-item">
                                    <a href="{{ action('PagesController@displayArchivedPublications', ['year' => 2013, 'type' => 'canteen']) }}">
                                        Rok <span class="filter_year">2013</span> - Faktúry ŠJ
                                    </a>
                                </li>


                                <li class="list-group-item">
                                    <a href="{{ action('PagesController@displayArchivedPublications', ['year' => 2012, 'type' => 'school']) }}">
                                        Rok <span class="filter_year">2012</span> - Škola
                                    </a>
                                </li>                                
                                <li class="list-group-item">
                                    <a href="{{ action('PagesController@displayArchivedPublications', ['year' => 2012, 'type' => 'canteen']) }}">
                                        Rok <span class="filter_year">2012</span> - Faktúry ŠJ
                                    </a>
                                </li>

                                <li class="list-group-item">
                                    <a href="{{ action('PagesController@displayArchivedPublications', ['year' => 2011, 'type' => 'school']) }}">Rok <span class="filter_year">2011</span> - Škola</a>
                                </li>
                                <li class="list-group-item">
                                    <a href="{{ action('PagesController@displayArchivedPublications', ['year' => 2011, 'type' => 'canteen']) }}">Rok <span class="filter_year">2011</span> - Faktúry ŠJ</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
