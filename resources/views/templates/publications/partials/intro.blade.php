<div class="panel-heading">
    <h2>Zverejňovanie zmlúv, faktúr, objednávok</h2>
</div>
<div class="panel-body">
    Novela Občianskeho zákonníka č. 546/2010, ktorou sa dopĺňa zákon č. 40/1964 Zb. Občiansky zákonník v znení neskorších predpisov a ktorým sa menia a dopĺňajú niektoré zákony, ukladá všetkým povinným subjektom, medzi ktoré patria aj samosprávy, zverejňovať zmluvy, faktúry a objednávky na tovary a služby na svojom webovom sídle s účinnosťou od 01. 01. 2011. 
</div>
