@extends('layouts.page')

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('components/bootstrap/dist/css/bootstrap.min.css') }}"/>
@endsection

@section('content')

    <div class="divider"></div>

    <div class="wrapper">
        <div class="container truncated">
            <div class="row">
                <div class="col-md-12">
                    @include('templates.publications.partials.back')
                    <div class="panel panel-default">

                        <div class="panel-body">
                            <strong>Zverejňovanie: Rok {{ $publication->year }} - {{ $publication->getTypeFullname() }}</strong>
                            <br><br><br>

                            {!! file_get_contents($publication->path) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
