<div id="header_wrapper">
	<div id="header_content">

		<ul class="main_menu">

			<a href="/"> <li>Úvod</li> </a>

			<a href="uvod" class="dropdown"> <li>O škole</li> </a>
			<ul class="dropdown uvod">
				<a href="/historia-skoly">
					<li>História školy</li>
				</a>
				<a href="/denny-rezim-skoly">
					<li>Denný režim</li>
				</a>
				<a href="/harmonogram">
					<li>Harmonogram rodičovských združení a pedagogických rád</li>
				</a>
				<a href="/zamestnanci">
					<li>Zamestnanci</li>
				</a>
				<a href="/triedy">
					<li>Triedy</li>
				</a>
				<a href="https://moja.skolanawebe.sk/skola/tomke" target="_blank">
					<li>Internetová žiacka knižka</li>
				</a>
				<a href="/uspechy-skoly">
					<li>Úspechy školy</li>
				</a>
				<a href="/rada-skoly">
					<li>Rada školy</li>
				</a>
				<a href="/rodicovske-zdruzenie">
					<li>Rodičovské združenie</li>
				</a>
				<a href="/vychovne-poradenstvo">
					<li>Výchovné poradenstvo</li>
				</a>
				<a href="/plavaren">
					<li>Plaváreň</li>
				</a>
				<a href="/o-sporte">
					<li>Úspešní absolventi</li>
				</a>
			</ul>

			<a href="kontakt" class="dropdown"> <li>Kontakt</li> </a>
			<ul class="dropdown kontakt">
				<a href="/kontakty-zamestnanci"><li>Zamestnanci</li></a>
				<a href="/kontakty-skola"><li>Škola</li></a>
			</ul>

			<a href="dokumenty" class="dropdown"> <li>Dokumenty školy</li> </a>
			<ul class="dropdown dokumenty">
				<a href="/dokumenty-skoly"><li>Dokumenty školy</li></a>
				<a href="/skolsky-poriadok"><li>Školský poriadok</li></a>
				<a href="/zverejnovanie"><li>Zverejňovanie zmlúv, faktúr a objednávok</li></a>
				<a href="/pracovne-miesta"><li>Voľné pracovné miesta</li></a>
				<a href="/verejneobstaravanie"><li>Verejné obstarávanie</li></a>
			</ul>

			<a href="pre-ziakov" class="dropdown"> <li>Pre žiakov</li> </a>
			<ul class="dropdown pre-ziakov">
				<a href="/cvicenia-z-matematiky">
				<li>Online cvičenia z matematiky</li></a>
				<a href="/cvicenia-z-informatiky">
				<li>Online cvičenia z informatiky</li></a>
				<a href="/prazdniny">
				<li>Prázdniny</li></a>
				<a href="/ziacky-parlament">
				<li>Žiacky parlament</li></a>
				<a href="/cistota-tried">
				<li>Čistota tried</li></a>
			</ul>

			<a href="fotoalbum" class="dropdown"> <li>Fotoalbum</li> </a>
			<ul class="dropdown fotoalbum">
				@if(isset($album_years))
					@foreach($album_years as $album_year)
						<a href="/albums/{{ $album_year }}">
							<li>{{ $album_year }}</li>
						</a>
					@endforeach
				@endif
			</ul>

			<a href="aktivity-skoly" class="dropdown"> <li>Aktivity školy</li> </a>
			<ul class="dropdown aktivity-skoly">
				<a href="/temp/aktivity">
					<li>Prehľad školských aktivít</li>
				</a>
				<a href="/erasmusplus">
					<li>Erasmus +</li>
				</a>
				<a href="/experimentovanieponemecky">
					<li>"Experimentovanie po nemecky"</li>
				</a>
				<a href="/projektyskoly">
					<li>Comenius 2012 - 2014</li>
				</a>
				<a href="/kosickemoderneskoly">
					<li>Košické moderné školy</li>
				</a>
				<a href="/comenius">
					<li>Comenius</li>
				</a>
			</ul>

		</ul>

		<div class="divider"></div>
		
	</div>
</div>
