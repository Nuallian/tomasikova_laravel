<div id="squares_index">

	<div class="square">
		<a href="/jedalen">
			<h3>Školská jedáleň</h3>
			<img src="{{ URL::asset('img/squares_index/jedalen.png') }}" class="img_centered">
			<span>
				Jedálny lístok
				<br>
				Informácie pre stravníkov
			</span>
			<a href="/jedalen" class="underlined padded">
				Čítať ďalej
			</a>
		</a>
	</div>

	<div class="square">
		<a href="/oznamy">
			<h3>Oznamy</h3>
			<img src="{{ URL::asset('img/squares_index/oznamy.png') }}" class="img_centered">
			<span>
				Riaditeľské voľno
				<br>
				Zmena vyučovania
			</span>
			<a href="/oznamy" class="underlined padded">
				Čítať ďalej
			</a>
		</a>
	</div>

	<div class="square">
		<a href="/skolsky-klub-deti">
			<h3>ŠKD</h3>
			<img src="{{ URL::asset('img/squares_index/skd.png') }}" class="img_centered">
			<a href="/skolsky-klub-deti" class="underlined padded">
				Čítať ďalej
			</a>
		</a>
	</div>

	<div class="square">
		<a href="/cvc">
			<h3>CVČ</h3>
			<img src="{{ URL::asset('img/squares_index/cvc.png') }}" class="img_centered">
			<span>
				Informácie o platbe
				<br>
				Ponuka ZÚ
			</span>
			<a href="/cvc" class="underlined padded">
				Čítať ďalej
			</a>
		</a>
	</div>

</div>

<div class="divider-lg">
	@include('partials.stat_counter')
</div>
