<div id="squares">

	<div class="square">
		<a href="/jedalen">
			<h3>Školská jedáleň</h3>
			<img src="{{ URL::asset('img/squares/jedalen.png') }}" class="img_centered">
		</a>
	</div>

	<div class="square">
		<a href="/oznamy">
			<h3>Oznamy</h3>
			<img src="{{ URL::asset('img/squares/oznamy.png') }}" class="img_centered">
		</a>
	</div>

	<div class="square">
		<a href="/skolsky-klub-deti">
			<h3>ŠKD</h3>
			<img src="{{ URL::asset('img/squares/skd.png') }}" class="img_centered">
		</a>
	</div>

	<div class="square">
		<a href="/cvc">
			<h3>CVČ</h3>
			<img src="{{ URL::asset('img/squares/cvc.png') }}" class="img_centered">
		</a>
	</div>

</div>
