@extends("layouts.admin")

@section('head')
    <script type="text/javascript" src="{{ URL::asset('ckeditor/ckeditor.js') }}"></script>
@endsection

@section("content")

    <div class="page-header">
        <h2>Editácia aktuality</h2>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <form method="POST" action="{{ route('article.update', $article->id) }}" id="page_form" enctype="multipart/form-data">
                <input name="_method" type="hidden" value="PUT">

                    {!! csrf_field() !!}

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3>Nadpis</h3>
                        </div>
                        <div class="panel-body">
                            <input
                                type="text"
                                name="title"
                                placeholder="Nadpis vložte tu"
                                class="form-control"
                                value="{{ $article->title }}"
                            >
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3>Text aktuality</h3>
                        </div>
                        <div class="panel-body">
                            <textarea id="editable">
                                @if($article->body)
                                    {{ $article->body }}
                                @endif
                            </textarea>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3>Obrázok ku nadpisu</h3>
                        </div>
                        <div class="panel-body">
                            <input type="file" name="image">
                        </div>
                    </div>

                    <input type="hidden" value="" id="contents_for_saving" name="contents_for_saving" />

                    <button id="save_article" type="submit" class="btn btn-primary">Uložiť aktualitu</button>
                </form>

            </div>
        </div>
    </div>

@endsection
