@extends('layouts.page')

@section('content')

    <div class="divider"></div>

    <div class="wrapper">

        <div class="page fullpage">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2 style="font-size:24px"><span style="font-size:30px">A</span>ktivity školy</h2>

                        <ul class="no-padding buffer-top-md">
                            @foreach($articles as $article)
                                <li>
                                    <div>
                                        <img src="{{ env('BASE_URL') . $article->img_path }}">
                                        <span style="color:#FF9900;font-weight:bold;vertical-align:bottom;font-size:16px;font-family:Tahoma;">
                                            {{ $article->title }}
                                        </span>                                    
                                    </div>
                                    <p class="width_100">
                                        {!! $article->body !!}
                                    </p>
                                </li>
                            @endforeach
                        </ul>

                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    {!! $articles->links() !!}
                </div>
            </div>
        </div>

    </div>

@endsection
