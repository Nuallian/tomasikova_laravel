@extends("layouts.admin")

@section("content")

	<div class="page-header">
		<h2>Editujete obrázky na uvítacej stránke </h2>
	</div>

	<div class="container">

		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3>Aktuálne obrázky na úvode</h3>
					</div>
					<div class="panel-body">

						@if(count($images) != 0)

							@foreach($images as $image)
								<div class="panel">
									<div class="panel-heading">
										<a href="/image/delete/{{ $image->name }}/carousel" class="btn btn-danger confirm-deletion delete-link" data-subject="obrázok">Vymazať obrázok</a>
									</div>
									<div class="panel-body">
										<img src="{{ URL::asset('photos/carousel_images/'. $image->name . '.jpg') }}" class="img_centered" width="400">
									</div>
								</div>
							@endforeach

						@else
							Žiadne obrázky na úvodnej stránke

						@endif

					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3>Nahrávanie nových obrázkov</h3>
					</div>
					<div class="panel-body">
						<form action="/carousel/store" class="dropzone" id="carousel-dropzone">
							{!! csrf_field() !!}
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection
