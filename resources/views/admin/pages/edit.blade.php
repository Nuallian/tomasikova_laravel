@extends("layouts.admin")

@section('head')
	<script type="text/javascript" src="{{ URL::asset('ckeditor/ckeditor.js') }}"></script>
@endsection

@section("content")

	<div class="page-header">
		<h2>Editujete stránku: {{ $model->fullname }}</h2>
	</div>

	<br />
	<br />
	<br />

		<h2>Obsah</h2>
		
		<div class="jumbotron">

			Štýly tabuľky:

			<input type="radio" name="table_style" value="classic" id="classic" class="tbl_select">
			<label for="classic">klasická: <img src="{{ URL::asset('img/table_styles/classic.png') }}" width="150"></label>

			<input type="radio" name="table_style" value="minimal" id="minimal" class="tbl_select">
			<label for="minimal">minimálna: <img src="{{ URL::asset('img/table_styles/minimal.png') }}" width="150"></label>

			<input type="radio" name="table_style" value="small" id="small" class="tbl_select">
			<label for="small">malá: <img src="{{ URL::asset('img/table_styles/small.png') }}" width="150"></label>

			<input type="radio" name="table_style" value="invisible" id="invisible" class="tbl_select">
			<label for="invisible">neviditeľná: <img src="{{ URL::asset('img/table_styles/invisible.png') }}" width="150"></label>

			<textarea rows="10" cols="80" id="editable">
				@if($content)
					{{ $content->body }}
				@endif
			</textarea>	

		</div>

	<div class="form-group submit_button">

		<form action="/edit/save" method="post" id="page_form">

			{!! csrf_field() !!}

			@if($content)
				<input type="hidden" value="{{ $content->body }}" id="contents_for_saving" name="contents_for_saving" />
			@else
				<input type="hidden" value="" id="contents_for_saving" name="contents_for_saving" />
			@endif

			<input type="hidden" value="{{ $model->name }}" id="model_name" name="model_name" />

			<button class="btn btn-success" type="submit" id="save_changes">Nahrať zmeny</button>

		</form>

	</div>

@endsection
