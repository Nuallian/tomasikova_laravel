@extends('layouts.admin')

@section('content')

	<div class="page-header">
		<h2>Vytvorenie nového súboru</h2>
	</div>

	<div class="container">

		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3>Pridanie nového súboru</h3>
					</div>
					<div class="panel-body">

						<div class="form-group">
							<form method="post" action="/file" class="dropzone" id="file-dropzone" enctype="multipart/form-data">

								{!! csrf_field() !!}

								<div class="input-group">
									<span class="input-group-addon" id="basic-addon1">Názov súboru: </span>
									<input type="text" class="form-control" aria-describedby="basic-addon1" name="file_name" id="file_name">
								</div><br />

								<button class="btn btn-success" type="submit">
									Uložiť súbor
								</button>

							</form>
						</div>

					</div>
				</div>
			</div>
		</div>

	</div>

@endsection
