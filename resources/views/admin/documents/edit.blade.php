@extends('layouts.admin')

@section('content')

	<div class="page-header">
		<h2>Editujete dokument: {{ $document->name }}</h2>
		<small>Stránka: {{ $document->page->fullname }}</small>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-12">
			
				<a href="/document/{{ $document->id }}/destroy" class="btn btn-danger confirm-deletion" data-subject="dokument">
					Vymazať dokument
				</a>
				
			</div>
		</div>
	</div>
@endsection
