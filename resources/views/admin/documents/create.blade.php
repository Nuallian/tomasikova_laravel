@extends('layouts.admin')

@section('content')

	<div class="page-header">
		<h2>Vytvorenie nového dokumentu</h2>
	</div>

	<div class="container">

		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3>Pridanie nového dokumentu</h3>
					</div>
					<div class="panel-body">

						<div class="form-group">
							<form method="post" action="/document" class="dropzone" id="document-dropzone" enctype="multipart/form-data">

								{!! csrf_field() !!}

								<div class="input-group">
									<span class="input-group-addon" id="basic-addon1">Názov dokumentu: </span>
									<input type="text" class="form-control" aria-describedby="basic-addon1" name="document_name" id="document_name">
								</div><br />

								<div class="input-group">
									<span class="input-group-addon" id="basic-addon1">Stránka dokumentu </span>
									<select class="form-control" aria-describedby="basic-addon1" name="page_id" id="page_id">
										<option selected disabled>---</option>
										@foreach($pages as $page)
											@if($page->isExtendableByDocument($page->id) == true)
												<option value="{{ $page->id }}">{{ $page->fullname }}</option>
											@endif
										@endforeach
									</select>
								</div><br />

								<button class="btn btn-success" type="submit">
									Vytvoriť nový dokument
								</button>

							</form>
						</div>

					</div>
				</div>
			</div>
		</div>

	</div>

@endsection
