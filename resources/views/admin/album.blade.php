@extends("layouts.admin")

@section("content")

	<div class="page-header">
		<h2>Editácia albumu: {{ $album->name }} / <small>{{ $album->year }}</small></h2>
	</div>

	<div class="container">

		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3>Aktuálne obrázky v albume <small>{{ $album->name }}</small></h3>
					</div>
					<div class="panel-body">
						@if(count($images) != 0)
							<div class="row">

								@foreach($images as $image)
									<div class="col-md-2">
										<div class="thumbnail">
											<img src="{{ URL::asset('photos/albums/thumbs/'. $image->filename . '.jpg') }}">
											<div class="caption">
												<p>
													<a href="/image/delete/{{ $image->filename }}/photo" class="btn btn-danger delete-link confirm-deletion" role="button" data-subject="obrázok">Vymazať obrázok</a>
												</p>
											</div>
										</div>
									</div>
								@endforeach

							</div>
						@else
							Žiadne obrázky v tomto albume
						@endif
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">

					<div class="panel-heading">
						<h3>Nahrávanie nových obrázkov do tohto albumu</h3>
					</div>

					<div class="panel-body">
						<form action="/photo/store" class="dropzone" id="carousel-dropzone">
							{!! csrf_field() !!}
							<input type="hidden" name="photoalbum_id" value="{{ $album->id }}">
						</form>
					</div>

				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3>Vymazanie fotoalbumu</h3>
					</div>
					<div class="panel-body">
						<a href="/album/{{ $album->id }}/delete/" class="btn btn-danger confirm-deletion" data-subject="fotoalbum">Vymazať fotoalbum</a>
					</div>
				</div>
			</div>
		</div>
		
	</div>

@endsection
