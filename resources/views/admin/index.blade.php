@extends("layouts.admin")

@section("content")

	<h1>Prihlásiť sa</h1>

	<div class="container">
	    <div class="row">
	        <div class="col-md-8 col-md-offset-2">
	            <div class="panel panel-default">
	                <div class="panel-heading">Prihlásenie sa</div>
	                <div class="panel-body">
	                    <form class="form-horizontal" role="form" method="POST" action="/auth">

	                        {!! csrf_field() !!}

	                        <div class="form-group">
	                            <label class="col-md-4 control-label">E-mailová adresa</label>

	                            <div class="col-md-6">
	                                <input type="email" class="form-control" name="email">
	                            </div>
	                        </div>

	                        <div class="form-group">
	                            <label class="col-md-4 control-label">Heslo</label>

	                            <div class="col-md-6">
	                                <input type="password" class="form-control" name="password">
	                            </div>
	                        </div>

	                        <div class="form-group">
	                            <div class="col-md-6 col-md-offset-4">
	                                <button type="submit" class="btn btn-primary">
	                                    <i class="fa fa-btn fa-sign-in"></i> Prihlásiť sa
	                                </button>
	                            </div>
	                        </div>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

@endsection
