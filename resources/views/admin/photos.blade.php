@extends("layouts.admin")

@section("content")

	<div class="page-header">
		<h2>Vytvorenie nového fotoalbumu</h2>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3>Fotoalbum</h3>
					</div>
					<div class="panel-body">
						<div class="form-group">

							<form method="post" action="/album/store">
								{!! csrf_field() !!}

								<div class="input-group">
									<span class="input-group-addon" id="basic-addon1">Názov albumu: </span>
									<input type="text" class="form-control" aria-describedby="basic-addon1" name="album_name">
								</div>

								<br />

								<div class="input-group">
									<span class="input-group-addon" id="basic-addon1">Školský rok: </span>
									<input type="text" placeholder="Vo formáte: 2015/2016" class="form-control" aria-describedby="basic-addon1" name="album_year">
								</div>

								<br />

								<button class="btn btn-success" type="submit">
									Vytvoriť nový album
								</button>

							</form>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection
