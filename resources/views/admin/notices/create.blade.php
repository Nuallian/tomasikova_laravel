@extends('layouts.admin')

@section('head')
	<script type="text/javascript" src="{{ URL::asset('ckeditor/ckeditor.js') }}"></script>
@endsection

@section('content')

	<div class="page-header">
		<h2>Vytvorenie nového oznamu</h2>
	</div>

	<div class="container">

		<div class="row">
			<div class="col-md-12">

				<form method="post" action="/notice" id="page_form">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3>Pridanie nového oznamu</h3>
						</div>
						<div class="panel-body">

							<div class="form-group">

								{!! csrf_field() !!}
	
								<div class="input-group">
									<span class="input-group-addon" id="basic-addon1">Stránka oznamu</span>
									<select class="form-control" aria-describedby="basic-addon1" name="page_id" id="page_select">
										<option selected disabled>---</option>
										@foreach($pages as $page)
											@if($page->hasNoNoticeOrDocuments($page->id))
												<option value="{{ $page->id }}">{{ $page->fullname }}</option>
											@endif
										@endforeach
									</select>
								</div><br />

							</div>

						</div>
					</div>

					<div class="panel panel-default">
						<div class="panel-heading">
							<h3>Text oznamu</h3>
						</div>
						<div class="panel-body">
							<textarea id="editable"></textarea>
						</div>
					</div>

					<input type="hidden" value="" id="contents_for_saving" name="contents_for_saving" />
					<button class="btn btn-success" type="submit" id="save_changes" data-model="notice">Vytvoriť oznam</button>

				</form>

			</div>
		</div>

	</div>

@endsection
