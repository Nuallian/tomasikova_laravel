@extends('layouts.admin')

@section('head')
	<script type="text/javascript" src="{{ URL::asset('ckeditor/ckeditor.js') }}"></script>
@endsection

@section('content')

	<div class="page-header">
		<h2>Editujete dokument: {{ $notice->page->fullname }}</h2>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<form method="POST" action="/notice/{{ $notice->id }}" id="page_form">

					<input name="_method" type="hidden" value="PUT">

					<div class="panel panel-default">
						<div class="panel-heading">
							<h3>Stránka oznamu</h3>
						</div>
						<div class="panel-body">

							<div class="form-group">

								{!! csrf_field() !!}
				
								<div class="input-group">
									<span class="input-group-addon" id="basic-addon1">Stránka oznamu</span>
									<select class="form-control" aria-describedby="basic-addon1" name="page_id" id="page_select">
										<option selected disabled>---</option>
										@foreach($pages as $page)
											<option value="{{ $page->id }}">{{ $page->fullname }}</option>
										@endforeach
									</select>
								</div><br />

							</div>

						</div>
					</div>

					<div class="panel panel-default">
						<div class="panel-heading">
							<h3>Text oznamu</h3>
						</div>
						<div class="panel-body">
							<textarea id="editable">
								@if($notice->content)
									{{ $notice->content }}
								@endif
							</textarea>
						</div>
					</div>

					<input type="hidden" value="" id="contents_for_saving" name="contents_for_saving" />

					<button class="btn btn-success" type="submit" id="save_changes" data-model="notice">Aktualizovať oznam</button>
					<a href="/notice/{{ $notice->id }}/destroy" class="btn btn-danger confirm-deletion pull-right" data-subject="oznam"> Vymazať oznam </a>

				</form>

			</div>
		</div>
	</div>

@endsection
