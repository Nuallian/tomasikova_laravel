@extends("layouts.admin")

@section("content")

	<div class="page-header">
		<h1>Administrátorské sekcia</h1>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<ul class="nav nav-pills nav-stacked">
				  <li role="presentation" id="page"><a href="{{ url('/admin/page') }}">Stránky</a></li>
				  <li role="presentation" id="document"><a href="{{ url('/admin/document') }}">Dokumenty</a></li>
				  <li role="presentation" id="album"><a href="{{ url('/admin/album') }}">Fotoalbumy</a></li>
				  <li role="presentation" id="notice"><a href="{{ url('/admin/notice') }}">Oznamy</a></li>
				  <li role="presentation" id="carouselPictures"><a href="{{ url('/admin/carouselPictures') }}">Obrázky na úvode stránky</a></li>
				  <li role="presentation" id="file"><a href="{{ url('/admin/file') }}">Ostatné súbory</a></li>
				  <li role="presentation" id="news"><a href="{{ url('/admin/news') }}">Aktuality</a></li>
				</ul>
			</div>

			<div class="col-md-9" id="admin-content">
				<div class="panel panel-default panel_page dashboard_panel">

					<div class="panel-heading">
						<h3>Stránky</h3>
					</div>

					<div class="panel-body no-padding">
						<ul class="list-group my-list-group">
							
							@foreach($pages as $page)

								<a href="/{{ $page->name }}/edit">
									<li class="list-group-item">{{ $page->fullname }}</li>
								</a>

							@endforeach

						</ul>
					</div>
				</div>

				<div class="panel panel-default panel_document dashboard_panel">
					<div class="panel-heading">
						<h3>Dokumenty</h3>
					</div>

					<div class="panel-body no-padding">
						<ul class="list-group my-list-group">

							<li class="list-group-item">
								<a href="/document/create" class="btn btn-success">
									+ pridať nový dokument
								</a>
							</li>

							@if($documents)
								@foreach($documents as $document)

									<a href="/document/{{ $document->id }}/edit">
										<li class="list-group-item">{{ $document->name }} <small>{{ $document->page->fullname }}</small></li>
									</a>

								@endforeach
							@endif

						</ul>
					</div>
				</div>

				<div class="panel panel-default panel_album dashboard_panel">
					<div class="panel-heading">
						<h3>Fotoalbumy <small>a fotky</small></h3>
					</div>

					<div class="panel-body no-padding">
						<ul class="list-group my-list-group">

							<li class="list-group-item">
								<a href="/album/create" class="btn btn-success">
									+ vytvoriť nový album
								</a>
							</li>

							@foreach($albums as $album)

								<a href="/album/{{ $album->name }}/edit">
									<li class="list-group-item">{{ $album->name }} <small>{{$album->year  }}</small></li>
								</a>

							@endforeach
						</ul>
					</div>
				</div>

				<div class="panel panel-default panel_carouselPictures dashboard_panel">
					<div class="panel-heading">
						<h3>Obrázky na úvode stránky</h3>
					</div>

					<div class="panel-body no-padding">
						<ul class="list-group my-list-group">
							<li class="list-group-item">
								<a href="/carousel/update" class="btn btn-warning">
									Zmeniť obrázky na úvode stránky
								</a>
							</li>
						</ul>
					</div>
				</div>

				<div class="panel panel-default panel_notice dashboard_panel">
					<div class="panel-heading">
						<h3>Oznamy <small>na stránkach</small></h3>
					</div>
					<div class="panel-body no-padding">
						<ul class="list-group my-list-group">

							@if($notices)
								@foreach($notices as $notice)
									<a href="/notice/{{ $notice->id }}/edit">
										<li class="list-group-item">
											Oznam stránky: {{ $notice->page->fullname }}
										</li>
									</a>
								@endforeach
							@endif

							<li class="list-group-item">
								<a href="/notice/create" class="btn btn-success">
									+ pridať nový oznam
								</a>
							</li>
						</ul>
					</div>
				</div>

				<div class="panel panel-default panel_file dashboard_panel">
					<div class="panel-heading">
						<h3>Ostatné súbory <small>kliknite pre skopírovanie linku</small></h3>
					</div>

					<div class="panel-body no-padding">

						<ul class="list-group my-list-group">

							@if($files)
								@foreach($files as $file)
									<a href="#" class="copy-content" data-path="{{ getenv('BASE_URL') . $file->path }}">
										<li class="list-group-item">
										
											<div class="row">
												<div class="col-md-8">
													{{ $file->name }}
													<small><a href="{{ $file->path }}" target="_blank">prezrieť / stiahnuť súbor</a></small>
												</div>
												<div class="col-md-4">
													<a href="/file/{{ $file->id }}/destroy" class="pull-right confirm-deletion" data-subject="súbor">
														vymazať <i class="fa fa-times fa-lg"></i>
													</a>
												</div>
											</div>
										</li>
									</a>
								@endforeach
							@endif

							<span class="copy" style="display:none;visibility:hidden;"></span>

							<li class="list-group-item">
								<a href="/file/create" class="btn btn-success">
									+ pridať nový súbor
								</a>
							</li>
						</ul>

					</div>
				</div>

				<div class="panel panel-default panel_news dashboard_panel">
					<div class="panel-heading">
						<h3>Aktuality</h3>
					</div>
					<div class="panel-body no-padding">

						<ul class="list-group my-list-group">

							@if($articles)
								@foreach($articles as $article)
									<a href="#">
										<li class="list-group-item">
										
											<div class="row">
												<div class="col-md-8">
													<a href="{{ route('article.edit', $article->id) }}">{{ $article->title }}</a>
												</div>
												<div class="col-md-4">
													<a href="{{ url('/article/'.$article->id.'/destroy') }}" class="pull-right confirm-deletion" data-subject="aktualitu">
														vymazať <i class="fa fa-times fa-lg"></i>
													</a>
												</div>
											</div>
										</li>
									</a>
								@endforeach
							@endif

							<li class="list-group-item">
								<a href="/article/create" class="btn btn-success">
									+ pridať novú aktualitu
								</a>
							</li>
						</ul>
					</div>
				</div>

			</div>
		</div>
	</div>
@endsection
