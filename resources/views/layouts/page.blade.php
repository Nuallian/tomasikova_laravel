<!DOCTYPE html>
<html>
<head>
	
	@include('partials.head')
	
	@yield('head')

	<link rel="stylesheet" type="text/css" href="{{ URL::asset('components/normalize-css/normalize.css') }}"/>
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('components/lightbox2/dist/css/lightbox.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('components/slick-carousel/slick/slick.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('components/slick-carousel/slick/slick-theme.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/app.css') }}">

	@include('partials.google_fonts')

</head>
	<body>

		<div class="content">

			<div id="logo"></div>

			@include('partials.header')

			@include('partials.squares')

		</div>

		@yield('content')

		<script type="text/javascript" src="{{ URL::asset('components/jquery/dist/jquery.min.js') }}"></script>
		<script type="text/javascript" src="{{ URL::asset('components/lightbox2/dist/js/lightbox.min.js') }}"></script>
		<script type="text/javascript" src="{{ URL::asset('components/slick-carousel/slick/slick.min.js') }}"></script>
		<script type="text/javascript" src="{{ URL::asset('js/app.js') }}"></script>
		<script type="text/javascript" src="{{ URL::asset('js/lightbox.js') }}"></script>

	</body>
</html>
