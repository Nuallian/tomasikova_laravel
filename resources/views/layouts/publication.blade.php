<!DOCTYPE html>
<html>
<head>

    @include('partials.head')
    
    <meta name="csrf_token" content="{{ csrf_token() }}">

    <link rel="stylesheet" type="text/css" href="{{ URL::asset('components/normalize-css/normalize.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('components/bootstrap/dist/css/bootstrap.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('components/sweetalert/dist/sweetalert.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('components/dropzone/dist/dropzone.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('components/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/app.css') }}">

    @yield('head')

</head>
<body class="admin">

    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">

                <a href="{{ route('dashboard') }}">
                    <span class="navbar-brand">
                        Admin obsahu

                        @if (Auth::user())
                            <small>{{ Auth::user()->name }}</small>
                        @endif

                    </span>
                </a>

                <a href="{{ url('/publication') }}">
                    <span class="navbar-brand">
                        Admin zverejňovania
                    </span>
                </a>

                <div class="navbar-right">
                    @if (Auth::user())
                        <a href="{{ route('logout') }}" class="btn btn-danger navbar-btn" style="float:right;">Odhlásiť sa</a>
                        <a href="{{ route('dashboard') }}" class="btn btn-warning navbar-btn">späť na index</a>
                    @endif
                </div>

            </div>
        </div>
    </nav>

    <div id="admin">

        @yield('content')
        
    </div>

    <script type="text/javascript" src="{{ URL::asset('components/jquery/dist/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('components/sweetalert/dist/sweetalert.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('components/dropzone/dist/dropzone.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/app.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/dropzone.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/admin.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/admin-publications.js') }}"></script>

</body>
</html>
