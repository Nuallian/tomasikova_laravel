@extends('layouts.publication')

@section('content')
    <div class="container" id="publication-form-container">
        {!! Form::open(['method' => 'POST', 'name' => 'publication_form_html', 'route' => 'publication.storeHtml', 'files'=>true]) !!}
            <h2>Nahrať HTML zverejnenia</h2>
            <h3>Typ: {{ $publication->getTypeFullname() }}</h3>
            <h3>Rok: {{ $publication->year }}</h3>

            {!! Form::hidden('publication_type', $publication->publication_type, NULL) !!}
            {!! Form::hidden('year', $publication->year, NULL) !!}
            {!! Form::file('file', ['class' => 'form-control buffer-top']) !!}

            {!! Form::submit('Nahrať zverejnenie', ['class' => 'btn btn-primary buffer-top']) !!}

        {!! Form::close() !!}
    </div>
@endsection
