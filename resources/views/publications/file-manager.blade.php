@extends('layouts.publication')

@section('content')
    <div class="container" id="publication-form-container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3>Manažér súborov zmlúv</h3>
                        <a href="#" id="file-manager-switch" class="btn btn-primary">Prehľad / upload súborov</a>
                    </div>
                    <div class="panel-body">

                        <div id="browser">

                            {!! Form::open(['method' => 'POST', 'name' => 'file-manager-filter-form', 'url' => '/publication/filter']) !!}
                                <select name="year-filter" class="form-control">
                                    <option selected>Filtrovať podľa roku</option>
                                    @foreach($years as $year)
                                        <option value="{{ $year->year }}">{{ $year->year }}</option>
                                    @endforeach
                                </select>
                            {!! Form::close() !!}

                            <div class="buffer-top">
                                <h3>Súbory:</h3>
                                <div class="well-lg files-holder"></div>
                            </div>

                        </div>

                        <div id="uploader" style="display:none;" class="buffer-top">
                            <h3>Nahrávanie nových súborov</h3>
                            <input type="text" name="year" class="form-control" placeholder="Rok zverejnenia">
                            <div id="publication-dropzone" class="dropzone buffer-top"></div>
                            <div class="dropzone-previews"></div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
