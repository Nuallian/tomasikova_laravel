{!! Form::hidden('publication_type', $type, NULL) !!}

<div class="row">
    <div class="col-md-12">
        <h2>Nahrať zverejnenie ručne</h2>
        <div class="input-group">
            <span class="input-group-addon" id="basic-addon1">Rok zverejnenia</span>
            {!! Form::text(
                'year',
                (isset($publication) ? $publication->year : NULL),
                [
                    'class'            => 'form-control',
                    'placeholder'      => 'Vo formáte: 2016',
                    'aria-describedby' => 'basic-addon1'
                ])
            !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="input-group">
            <span class="input-group-addon" id="basic-addon2">Dodávateľ</span>
            {!! Form::text(
                'contractor',
                (isset($publication) ? $publication->contractor : NULL),
                [
                    'class'            => 'form-control',
                    'aria-describedby' => 'basic-addon2'
                ])
            !!}
        </div>
    </div>
</div>

@if($type != 'contract')
    <div class="row">
        <div class="col-md-12">
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon5">Adresa dodávateľa</span>
                {!! Form::text(
                    'contractor_address',
                    (isset($publication) ? $publication->contractor_address : NULL),
                    [
                        'class'            => 'form-control',
                        'aria-describedby' => 'basic-addon5'
                    ])
                !!}
            </div>
        </div>
    </div>
@endif

@if($type == 'contract')
    <div class="row">
        <div class="col-md-12">
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon3">Číslo zmluvy</span>
                {!! Form::text(
                    'contract_number',
                    (isset($publication) ? $publication->contract_number : NULL),
                    [
                        'class'            => 'form-control',
                        'aria-describedby' => 'basic-addon3'
                    ])
                !!}
            </div>
        </div>
    </div>
@endif

@if($type == 'contract')
    <div class="row">
        <div class="col-md-12">
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon6">Predmet zmluvy</span>
                {!! Form::text(
                    'contract_subject',
                    (isset($publication) ? $publication->contract_subject : NULL),
                    [
                        'class'            => 'form-control',
                        'aria-describedby' => 'basic-addon6'
                    ])
                !!}
            </div>
        </div>
    </div>
@endif

@if($type == 'bill' || $type == 'canteen' || $type == 'order')
    <div class="row">
        <div class="col-md-12">
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon6">Interné číslo faktúry (Int.č.fa) / Číslo objednávky</span>
                {!! Form::text(
                    'internal_bill_number',
                    (isset($publication) ? $publication->internal_bill_number : NULL),
                    [
                        'class'            => 'form-control',
                        'aria-describedby' => 'basic-addon6'
                    ])
                !!}
            </div>
        </div>
    </div>
@endif

@if($type == 'contract')
    <div class="row">
        <div class="col-md-12">
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon4">Dátum</span>
                {!! Form::text(
                    'date',
                    (isset($publication) ? $publication->date : NULL),
                    [
                        'class'            => 'form-control',
                        'aria-describedby' => 'basic-addon4'
                    ])
                !!}
            </div>
        </div>
    </div>
@endif

@if($type == 'bill' || $type == 'canteen')
    <div class="row">
        <div class="col-md-12">
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon4">Faktúra došla dňa</span>
                {!! Form::text(
                    'bill_arrival_date',
                    (isset($publication) ? $publication->bill_arrival_date : NULL),
                    [
                        'class'            => 'form-control',
                        'aria-describedby' => 'basic-addon4'
                    ])
                !!}
            </div>
        </div>
    </div>
@endif

@if($type != 'contract')
    <div class="row">
        <div class="col-md-12">
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon4">IČO</span>
                {!! Form::text(
                    'ico_number',
                    (isset($publication) ? $publication->ico_number : NULL),
                    [
                        'class'            => 'form-control',
                        'aria-describedby' => 'basic-addon4'
                    ])
                !!}
            </div>
        </div>
    </div>
@endif

@if($type != 'contract')
    <div class="row">
        <div class="col-md-12">
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon4">Popis plnenia</span>
                {!! Form::text(
                    'fullfilment_description',
                    (isset($publication) ? $publication->fullfilment_description : NULL),
                    [
                        'class'            => 'form-control',
                        'aria-describedby' => 'basic-addon4'
                    ])
                !!}
            </div>
        </div>
    </div>
@endif

@if($type == 'canteen' || $type == 'bill')
    <div class="row">
        <div class="col-md-12">
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon4">Suma</span>
                {!! Form::text(
                    'price',
                    (isset($publication) ? $publication->price : NULL),
                    [
                        'class'            => 'form-control',
                        'aria-describedby' => 'basic-addon4'
                    ])
                !!}
            </div>
        </div>
    </div>
@endif

@if($type != 'contract')
    <div class="row">
        <div class="col-md-12">
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon4">Identifikácia zmluvy (ak to je relevantné)</span>
                {!! Form::text(
                    'identifier',
                    (isset($publication) ? $publication->identifier : NULL),
                    [
                        'class'            => 'form-control',
                        'aria-describedby' => 'basic-addon4'
                    ])
                !!}
            </div>
        </div>
    </div>
@endif

@if($type == 'order')
    <div class="row">
        <div class="col-md-12">
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon4">Dátum vyhotovenia objednávky</span>
                {!! Form::text(
                    'date_of_completion',
                    (isset($publication) ? $publication->date_of_completion : NULL),
                    [
                        'class'            => 'form-control',
                        'aria-describedby' => 'basic-addon4'
                    ])
                !!}
            </div>
        </div>
    </div>
@endif

@if($type == 'order')
    <div class="row">
        <div class="col-md-12">
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon4">Meno, priezvisko, funkcia zodpovednej osoby</span>
                {!! Form::text(
                    'responsible_person_credentials',
                    (isset($publication) ? $publication->responsible_person_credentials : NULL),
                    [
                        'class'            => 'form-control',
                        'aria-describedby' => 'basic-addon4'
                    ])
                !!}
            </div>
        </div>
    </div>
@endif

@if($type != 'contract')
    <div class="row">
        <div class="col-md-12">
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon4">Poznámka</span>
                {!! Form::text(
                    'note',
                    (isset($publication) ? $publication->note : NULL),
                    [
                        'class'            => 'form-control',
                        'aria-describedby' => 'basic-addon4'
                    ])
                !!}
            </div>
        </div>
    </div>
@endif

@if($type == 'contract')
    @if (isset($file))
        <a href="{{ url($file->path) }}" target="_blank" class="btn btn-success">Pozrieť súbor zverejnenia</a>
        <a href="{{ url('/publicationfile/delete/'.$file->id) }}" class="btn btn-danger">vymazať súbor zverejnenia</a>
    @endif
    <div id="publication-dropzone" class="dropzone"></div>
    <div class="dropzone-previews"></div>
@endif

{!! Form::submit('Uložiť zverejnenie', ['class' => 'btn btn-success submit_publication buffer-top']) !!}
