@extends('layouts.publication')

@section('content')
    <div class="container" id="publication-form-container">
        {!! Form::open(['method' => 'POST', 'name' => 'publication_form_html', 'route' => 'publication.storeHtml', 'files'=>true]) !!}
            <h2>Nahrať HTML zverejnenia</h2>

            {!! Form::hidden('publication_type', $type, NULL) !!}
            {!! Form::file('file', ['class' => 'form-control buffer-top']) !!}

            <div class="input-group buffer-top">
                <span class="input-group-addon" id="basic-addon1">Rok zverejnenia</span>
                {!! Form::number(
                    'year',
                    (isset($publication) ? $publication->year : NULL),
                    [
                        'class'            => 'form-control',
                        'placeholder'      => 'Vo formáte: 2016',
                        'aria-describedby' => 'basic-addon1'
                    ])
                !!}
            </div>
            {!! Form::submit('Nahrať zverejnenie', ['class' => 'btn btn-primary buffer-top']) !!}

        {!! Form::close() !!}
    </div>
@endsection
