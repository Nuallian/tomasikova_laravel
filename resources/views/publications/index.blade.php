@extends('layouts.publication')

@section('content')

    <div class="page-header">
        <h1>Zverejňovanie pre rok {{ $current_year }}</h1>

        <div class="text-center">
            <h3>Vyberte si rok zverejňovania</h3>
            @foreach($years as $year)
                <a href="{{ url('/publication/'.$year->year) }}">{{ $year->year }}</a>
            @endforeach
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default top">
                    <div class="panel-heading">
                        <h3>
                            <a href="#" id="choose-type">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                                Vytvoriť nové zverejnenie
                            </a>
                        </h3>
                        <h4>
                            <a href="{{ url('/publicationsfiles') }}">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                                Správa zmlúv
                            </a>
                        </h4>
                    </div>

                    @if (isset($orders) && count($orders) != 0)
                        <div class="panel-body orders">
                            <h2>Objednávky</h2>
                            @foreach($orders as $order)
                                <a href="{{ route('publication.edit', $order->id) }}">Editovať toto zverejnenie</a>
                            @endforeach
                        </div>
                    @endif

                    @if (isset($contracts) && count($contracts) != 0)
                        <div class="panel-body contracts">
                            <h2>Zmluvy</h2>
                            @foreach($contracts as $contract)
                                <a href="{{ route('publication.edit', $contract->id) }}">Editovať toto zverejnenie</a>
                            @endforeach
                        </div>
                    @endif

                    @if (isset($bills) && count($bills) != 0)
                        <div class="panel-body bills">
                            <h2>Faktúry</h2>
                            @foreach($bills as $bill)
                                <a href="{{ route('publication.edit', $bill->id) }}">Editovať toto zverejnenie</a>
                            @endforeach
                        </div>
                    @endif

                    @if (isset($canteens) && count($canteens) != 0)
                        <div class="panel-body canteen">
                            <h2>Zverejňovanie jedálne</h2>
                            @foreach($canteens as $canteen)
                                <a href="{{ route('publication.edit', $canteen->id) }}">Editovať toto zverejnenie</a>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
