validateNotices =  ->
	if $( 'input#document_name' ).val() is undefined or $( 'input#document_name' ).val() is "" or $( 'select#page_id' ).val() is null
		return false
	else
		return true

validateFiles = ->
	if $( 'input#file_name' ).val() is undefined or $( 'input#file_name' ).val() is ""
		return false
	else
		return true

$ ->
	# ---------------------------------
	# Dropzone for carousel images
	# ---------------------------------
	Dropzone.options.carouselDropzone =
		maxFilesize: 20,
		uploadMultiple: false,
		dictFileTooBig: 'Obrázok je väčší než 20MB',
		dictDefaultMessage: 'Ak chcete nahrať nové obrázky, potiahnite ich tu, alebo tu kliknite',
		headers:
			'X-CSRF-Token': $('meta[name="csrf_token"]').attr('content')
		accept : (file, done) ->
			done()

	# ---------------------------------
	# Dropzone for documents
	# ---------------------------------
	Dropzone.options.documentDropzone =
		autoProcessQueue: false,
		uploadMultiple: true,
		parallelUploads: 100,
		maxFiles: 1,
		maxFilesize: 20,
		autoProcessQueue: false,
		dictFileTooBig: 'Dokument je väčší než 20MB',
		dictDefaultMessage: 'Ak chcete nahrať nový dokument, potiahnite ho tu, alebo tu kliknite',
		headers:
			'X-CSRF-Token': $('meta[name="csrf_token"]').attr('content')
		init : ->
			
			myDropzone = this

			$("button[type=submit").on 'click', ( e ) ->
				e.preventDefault();
				e.stopPropagation();

				if validateNotices() is true
					myDropzone.processQueue();
				else
					swal 'Vyplňte všetky polia!', '', 'warning'
				
		success : ->
			swal
				title: 'Dokument bol nahratý',
				type: 'success',
				() ->
					window.location.href = '/admin';

	# ---------------------------------
	# Dropzone for files
	# ---------------------------------

	Dropzone.options.fileDropzone =
		autoProcessQueue: false,
		uploadMultiple: true,
		parallelUploads: 100,
		maxFiles: 1,
		maxFilesize: 20,
		autoProcessQueue: false,
		dictFileTooBig: 'Súbor je väčší než 20MB',
		dictDefaultMessage: 'Ak chcete nahrať nový súbor, potiahnite ho tu, alebo tu kliknite',
		headers:
			'X-CSRF-Token': $('meta[name="csrf_token"]').attr('content')
		init : ->

			myDropzone = this

			$("button[type=submit").on 'click', ( e ) ->
				e.preventDefault();
				e.stopPropagation();

				if validateFiles() is true
					myDropzone.processQueue();
				else
					swal 'Vyplňte všetky polia!', '', 'warning'
				
		success : ->
			swal
				title: 'Súbor bol nahratý',
				type: 'success',
				() ->
					window.location.href = '/admin'
