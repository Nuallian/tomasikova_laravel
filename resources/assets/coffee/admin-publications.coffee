$ ->
    publicationUploaded = () ->
        swal("Zverejnenie bolo uložené!", "Budete redirectnutý na stránku zverejnení", "success");

        setTimeout () ->
          window.location = "/publicationsfiles";
        ,2000

    showProgress = () ->
      swal
        title: "Súbory sa náhravajú",
        text: '<div class="progress progress-bar-info progress-bar-striped active" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0;"><div class="progress-bar" role="progressbar"><span class="sr-only" data-dz-uploadprogress></span></div></div>',
        showConfirmButton: false,
        showCancelButton: false,
        allowEscapeKey: false,
        html: true

    updateProgress = (progress) ->
      $('.progress').css "width", progress + "%"

    chooseType = () ->
        swal(
            title: "Vyberte si typ zverejnenia",
            text:  "<a href='/publication/create/canteen' id='type'>Zverejnenie jedálne</a><br /><a href='/publication/create/contract' id='type'>Zverejnenie zmluvy</a><br /><a href='/publication/create/bill' id='type'>Zverejnenie faktúry</a><br /><a href='/publication/create/order' id='type'>Zverejnenie objednávky</a>",
            html: true,
            showConfirmButton: false,
            allowOutsideClick: true
        )

    validateYear = (yearSelector) ->
        if yearSelector.val() is ''
            swal(
                title: "Musíte vložiť rok zverejnenia!",
                type: "error"
            )
        else
            true

    displayFile = (file) ->
        link = '<div class="single-file"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> ' + file.name + '<br />Cesta: ' + file.path + '</div>'
        $('.files-holder').append(link)

    refreshFiles = (files) ->
        $('.files-holder').html("")
        for i in [0...files.length]
            displayFile(files[i])

    submitFilterForm = (year) ->
        form = $('form[name="file-manager-filter-form"]:eq(0)')
        $.ajax
            url: form.attr 'action'
            method: form.attr 'method'
            data:
                year: year
            success: (files) ->
                refreshFiles(files)
            error: (response) ->
                console.log 'error :('
                console.log error

    fileManagerSwitch = () ->
        if $('#uploader').is(":visible")
            $('#uploader').hide()
            $('#browser').show()
        else
            $('#browser').hide()
            $('#uploader').show()       
        

    # choosing the type of publication
    $(document).on 'click', '#choose-type', (event) ->
        event.preventDefault()
        chooseType()

    ## -------------------------------------------------
    ## Config for dropzone for publication files
    ## -------------------------------------------------
    dropzone_publication_config =
        url: "/publication/storeFiles",
        autoProcessQueue: true,
        paramName: "file",
        maxFilesize: 20,
        addRemoveLinks: true,
        dictDefaultMessage: 'Súbory zverejnení potiahnite, alebo načítajte kliknutím tu',
        dictRemoveFile: 'Odstrániť súbor',
        headers:
            'X-CSRF-Token': $('meta[name="csrf_token"]').attr('content')
        totaluploadprogress: (progress) ->
            updateProgress(progress)
        processing: () ->
            showProgress()
        success: (data) ->
            publicationUploaded()
        error: (error) ->
            swal
                title: "Pri načítavaní súborov niekde nastala chyba",
                text: error,
                type: "error"

    ## ------------------------------
    ## Initialize the dropzone
    ## ------------------------------
    Dropzone.autoDiscover = false
    myDropzone

    if $('#publication-dropzone').length != 0
        myDropzone = new Dropzone "div#publication-dropzone", dropzone_publication_config

        # year field validation
        if myDropzone
            myDropzone.on 'sending', (file, xhr, formData) ->
                year = $('input[name="year"]').val()
                formData.append('year', year);
            myDropzone.processQueue()

    $(document).on 'click', 'form[name="publication_form_html"] input[type="submit"]', (event) ->
        event.preventDefault()

        if validateYear( $('form[name="publication_form_html"] input[name="year"]') )
            $('form[name="publication_form_html"]').submit()

    $(document).on 'click', '.jumpTo', (event) ->
        event.preventDefault()
        target = $(this).data('target')
        $('html, body').animate
            scrollTop: $("."+target).offset().top - 40
        , 500

    $(document).on 'change', 'select[name="year-filter"]', (event) ->
        event.preventDefault()

        value = event.target.value

        if ! isNaN(value)
            submitFilterForm(value)

    $(document).on 'click', '#file-manager-switch', (event) ->
        event.preventDefault()
        fileManagerSwitch()        
