validateNotices = ->
	if $( 'select#page_select' ).val() is null
		return false
	else
		return true

addContentsFromEditor = ->
	contents = CKEDITOR.instances.editable.getData()
	$('#contents_for_saving').val ''
	$('#contents_for_saving').val contents
	contents


if $('#editable').length != 0

	editor = CKEDITOR.replace 'editable',
		toolbar:
			[
				{ name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
				{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Link' ] },
				{ name: 'colors', items : [ 'TextColor', '-', 'BGColor' ] },
				{ name: 'paragraph', items : [ 'NumberedList', '-', 'BulletedList' ] },
				{ name: 'alignment', items: [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ]}
				'/',
				{ name: 'insert', items : [ 'Image','Table','HorizontalRule','Smiley','SpecialChar' ] },
				{ name: 'tools', items : [ 'Maximize' ] }
			]

	# Selecting table styles
	$('.tbl_select').on 'click', ->
		tbl_class = $(this).val()
		table = $( editor.document.$.getElementsByTagName('table') )
		
		table.removeClass()
		table.addClass(tbl_class)

$ ->

	$('#save_changes').on 'click', ( event ) ->
		event.preventDefault()

		$('table td')
			.addClass 'tbl_cll'
			.addClass 'tbl_center'
			
		$('table th')
			.addClass 'tbl_cll'
			.addClass 'tbl_center'

		addContentsFromEditor()

		if $( this ).attr( 'data-model' ) is 'notice'

			if validateNotices() is true
				$('#page_form').submit()
				swal 'Zmeny sa ukladajú', 'Po uložení zmien budete redirectnutý naspäť na index', 'success'
			else
				swal 'Vyplňte všetky polia', '', 'warning'

		else
			$('#page_form').submit()
			swal 'Zmeny sa ukladajú', 'Po uložení zmien budete redirectnutý naspäť na index', 'success'

	$('#save_article').on 'click', (event) ->
		event.preventDefault()
		addContentsFromEditor()
		$('#page_form').submit()
		swal 'Zmeny sa ukladajú', 'Po uložení zmien budete redirectnutý naspäť na index', 'success'
