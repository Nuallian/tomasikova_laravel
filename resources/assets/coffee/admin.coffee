# Deletion confirmation
$('.confirm-deletion').on 'click', ( event ) ->
	event.preventDefault()
	link    = $( this ).attr 'href'
	subject = $( this ).attr 'data-subject'

	swal
		title: "Ste si istý, že chcete " + subject + " zmazať?",
		text: "Ak " + subject + " zmažete, už sa to nedá vrátiť späť",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Zmazať",
		cancelButtonText: "Nezmazať",
		closeOnConfirm: false
		() ->
			swal "Zmazané", "", "success"
			window.location = link

# Copying links of uploaded files
$('.copy-content').on 'click', ( event ) ->
	event.preventDefault()
	path = $(this).attr 'data-path'

	swal
		title: "Označte a skopírujte tento link"
		text: path

$ ->
	if $('.nav-stacked').length > 0
		# initialize variables
		parameters = document.location.pathname
		contentHolderSelector = '#admin-content'
		category = parameters.replace("/admin/", "")
		contentToDisplay = ".panel_"+category;		

		# make the button active
		$('#'+category).addClass("active")

		# display the corresponding content from the url
		$(contentToDisplay).show()
