$ ->
	lightBoxOptions = {
		alwaysShowNavOnTouchDevices: true,
		albumLabel: "Obrázok %1 z %2",
		disableScrolling: true,
		fadeDuration: 100,
		positionFromTop: 70,
		resizeDuration: 200  }

	lightbox.option(lightBoxOptions)
