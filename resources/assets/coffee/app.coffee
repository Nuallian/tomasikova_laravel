$ ->
	initiateCarousel = ->
		# initiate carousel
		if $('#carousel').length != 0
			settings = { accessibility: true, adaptiveHeight: true, arrows: true, centerMode: true, fade: true, dots: true, pauseOnDotsHover: true, autoplay: true, autoplaySpeed: 8000 }
			$('#carousel').slick settings

	# toggle dropdown menus
	$( '#header_content a' ).on 'click', ( event ) ->
		if $( this ).hasClass( 'dropdown' )
			event.preventDefault()

		$('ul.dropdown').hide()

		dropdown_position = $(this).offset()
		href = $(this).attr('href').replace('/', '')
	
		if $(this).attr('href') != '/aktivity-skoly'						
			$('ul.dropdown.' + href).css("left", dropdown_position.left).fadeIn("fast")

	$(document).on 'click', (event) ->
		container = $( '#header_content a' )
		if !container.is(event.target) && container.has(event.target).length is 0
			$('ul.dropdown').hide()

	initiateCarousel()	
