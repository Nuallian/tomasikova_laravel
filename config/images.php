<?php

return [
	'full_size_upload_dir'   => env('FULL_SIZE_UPLOAD_DIR'),
	'thumb_upload_dir'       => env('THUMB_UPLOAD_DIR'),
	'carousel_upload_dir'    => env('CAROUSEL_UPLOAD_DIR'),
	'article_img_upload_dir' => env('ARTICLE_UPLOAD_DIR')
];
