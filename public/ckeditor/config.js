/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	config.language           = 'sk';
	config.height             = '500px';
	config.colorButton_colors = '005EBC,006BC3,4FDDFF,DDF8FF,79DA4C,E26620,FFF,000';
	config.extraPlugins       = 'notification,notificationaggregator,widget,uploadwidget,uploadimage,justify';
	config.imageUploadUrl     = '/textimage/store';
};
