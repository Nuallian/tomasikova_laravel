var gulp     = require( 'gulp' );
var sass     = require( 'gulp-sass' );
var gutil    = require( 'gulp-util' );
var notifier = require( 'gulp-notify' );
var watch    = require( 'gulp-watch' );
var jade     = require( 'gulp-jade' );
var coffee   = require( 'gulp-coffee' );

var paths = {

	sass: {
		src: 'resources/assets/sass/app.scss',
		watch: ['resources/assets/sass/*.scss', 'resources/assets/sass/*/*.scss'],
		dest: 'public/css'
	},

	coffee: {
		src: 'resources/assets/coffee/*.coffee',
		dest: 'public/js'
	},

	jade: {
		src: 'resources/views/*.jade',
		dest: 'public/views'
	}

};

var notify = function( error )
{
	console.log( error );
	notifier().write( error );
};

var tasks = {

	sass: function()
	{
		gulp.src( paths.sass.src )
			.pipe( sass().on( 'error', notify ) )
			.pipe( gulp.dest( paths.sass.dest ) );
	},

	coffee: function()
	{
		gulp.src( paths.coffee.src )
			.pipe( coffee().on( 'error', notify ) )
			.pipe( gulp.dest( paths.coffee.dest ) );
	},

	jade: function()
	{
		gulp.src( paths.jade.src )
			.pipe( jade( { pretty: true } ) )
			.on( 'error', notify )
			.pipe( gulp.dest( paths.jade.dest ) );
	}
}

gulp.task( 'default', function() {
	tasks.sass();
	tasks.coffee();
	tasks.jade();
});

gulp.task( 'watch', function() {
	watch( paths.sass.watch, tasks.sass );
	watch( paths.coffee.src, tasks.coffee );
	watch( paths.jade.src, tasks.jade );
});