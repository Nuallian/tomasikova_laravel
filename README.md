# Tomasikova frontend

This is the code base for the new webpage of Základná škola Tomášikova.
It is based on [Laravel](https://www.laravel.com).
This project uses [coffeescript](http://www.coffeescript.org) for javascript and [sass](http://www.sass-lang.com) for stylesheets.
Resources are compiled with [gulp](http://gulpjs.com/).


## Installation

* `git clone https://bitbucket.org/Nuallian/tomasikova-frontend`
* `cd tomasikova-frontend`
* `composer install`
* `npm install`
* `bower install`
* `gulp` to build from resources (then you can use `gulp watch` to watch for changes)
* change the document root to `public`
* create and change your `.env` with your database information according to `.env.example file` (do not forget to change the BASE_URL to your domain or IP address and app environment as well as debugging)
* run `php artisan key:generate` and copy the generated key into `.env` file, if it is not added automatically
* create a MySQL database
* migrate the tables by running `php artisan migrate`
* uncomment desired seeds in `DatabaseSeeder.php` and seed the database with `php artisan db:seed`
* ready to go!

## Instructions / caveats
* if you get internal server 500 error, you can try going `chmod -R 777` on `bootstrap/cache` and `storage`
* make sure that `public/photos/carousel_images`, `public/photos/full_size`, `public/photos/thumbs`, `public/documents/`, `public/text_images/`, `public/files` are created and writable, if not try going `chmod 777` on those folders
* if you are getting Internal Server Error on all other routes except `/`, try rewriting the RewriteBase directive in `.htaccess` to `RewriteBase /`
* make sure `storage/framework` is writeable as well, for the purpose of storing sessions and caching
* change your apache2 config to accept `.htaccess`
* add your test credentials to the `.env` file to be able to login a test admin user
* if you see a white page after doing everything else, try going `chmod -R gu+w` and `chmod -R guo+w` on the upper level of root directory (e.g. `/var/www`)
* make sure you can upload large body, by going `nano /etc/nginx/nginx.conf` and adding a line that reads `client_max_body_size 4M` or more to the `http`, `serve`, or `location` section
* you have to enable `a2enmod rewrite`, so do that in case routing is not working properly for you
* you can try doing `sudo locale-gen en_US.UTF-8` and `export LANG=en_US.UTF-8` if you have installation problems, because of trouble with setting locale on the machine you are installing this app on

## CKeditor

 This project uses CKeditor for editing pages
* to customize the editor, use `ckeditor/config.js`
* to customize the colors for text and background of the text, use `config.colorButton_colors = '<hex code of the color>;`
